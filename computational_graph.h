#ifndef COMPUTATIONAL_GRAPH_H
#define COMPUTATIONAL_GRAPH_H

#include <iostream>
#include <vector>
#include "mat.h"


namespace cg
{


class Var
{

public:

    double value;
    double grad;

    Var()
    {
        value = 0;
        grad = 0;       
    }

    Var(double value, double grad)
    {
        this->value = value;
        this->grad = grad;
    }
};


class ComputationalGraph
{

public:

    virtual double forward(std::vector<double> elements) = 0;
    virtual std::vector<double> backward(double d_out) = 0;

};


class Prod : public ComputationalGraph
{

private:

    std::vector<Var> x;

public:

    double forward(std::vector<double> elements)
    {
        double result = 1;
        for(int i = 0; i < elements.size(); i++)
        {
            x.push_back(Var());
            x[i].value = elements[i];
            result *= elements[i];
        }
        return result;
    }

    std::vector<double> backward(double d_out)
    {
        std::vector<double> result;
        for(int i = 0; i < x.size(); i++)
        {
            x[i].grad = 1;
            for(int j = 0; j < x.size(); j++)
            {
                if(j == i)
                    continue;
                x[i].grad *= x[j].value;
            }
            result.push_back(d_out * x[i].grad);
        }
        return result;
    }

};


class Sum : public ComputationalGraph
{

private:

    std::vector<Var> x;

public:

    double forward(std::vector<double> elements)
    {
        double result = 0;
        for(int i = 0; i < elements.size(); i++)
        {
            x.push_back(Var());
            x[i].value = elements[i];
            result += elements[i];
        }
        return result;
    }

    std::vector<double> backward(double d_out)
    {
        std::vector<double> result;
        for(int i = 0; i < x.size(); i++)
        {
            x[i].grad = 1;
            result.push_back(d_out * x[i].grad);
        }
        return result;
    }

};


class Diff : public ComputationalGraph
{

private:

    std::vector<Var> x;

public:

    double forward(std::vector<double> elements)
    {
        double result = 0;
        for(int i = 0; i < elements.size(); i++)
        {
            x.push_back(Var()); 
            x[i].value = elements[i];
            result -= elements[i];
            if(i == 0)
                result *= -1;
        }
        return result;
    }

    std::vector<double> backward(double d_out)
    {
        std::vector<double> result;
        for(int i = 0; i < x.size(); i++)
        {
            x[i].grad = -1;
            if(i == 0)
                x[i].grad *= -1;
            result.push_back(d_out * x[i].grad);
        }
        return result;
    }

};


class Max : public ComputationalGraph
{

private:

    std::vector<Var> x;
    double max;

public:

    double forward(std::vector<double> elements)
    {
        double result = 0;
        for(int i = 0; i < elements.size(); i++)
        {
            x.push_back(Var()); 
            x[i].value = elements[i];
            if(i == 0)
                result = elements[i];
            if(result < elements[i])
                result = elements[i];
        }
        max = result;
        return result;
    }

    std::vector<double> backward(double d_out)
    {
        int max_num = 0;
        for(int i = 0; i < x.size(); i++)
        {
            if(x[i].value == max)
            {
                x[i].grad = 1;
                max_num++;
                continue;
            }
            x[i].grad = 0;
        }

        for(int i = 0; i < x.size(); i++)
            x[i].grad /= max_num;

        std::vector<double> result(x.size(), 0);
        for(int i = 0; i < x.size(); i++)
        {
            result[i] = x[i].grad;
            result[i] *= d_out;
        }
        return result;
    }

};


class Power
{

private:

    Var x;
    double power;

public:

    Power(double power)
    {
        this->power = power;
    }

    double forward(double x)
    {
        this->x.value = x;
        return std::pow(x, power);
    }

    double backward(double d_out)
    {
        x.grad = 2 * x.value;
        x.grad = power * std::pow(x.value, power - 1);
        return d_out * x.grad;
    }
};


class Square
{

private:

    Power pow;

public:

    Square(): pow(2.0) {}

    double forward(double x)
    {
        return pow.forward(x);
    }

    double backward(double d_out)
    {
        return pow.backward(d_out);
    }
};


class Sqrt
{

private:

    Power pow;

public:

    Sqrt(): pow(1.0/2.0) {}

    double forward(double x)
    {
        return pow.forward(x);
    }

    double backward(double d_out)
    {
        return pow.backward(d_out);
    }
};


class Divide
{

private:

    Var x;
    Power pow;
    Prod prod;

public:

    Divide(): pow(-1) {}

    double forward(double x1, double x2)
    {
        return prod.forward(std::vector<double>{x1, pow.forward(x2)});
    }

    std::vector<double> backward(double d_out)
    {
        return std::vector<double>{prod.backward(d_out)[0], prod.backward(pow.backward(d_out))[1]};
    }
};


class Distance
{

private:

    int samples_num;
    std::vector<std::vector<std::shared_ptr<Square>>> l1;
    std::vector<std::vector<std::shared_ptr<Sum>>> l2;
    std::vector<std::vector<std::shared_ptr<Sqrt>>> l3;
    std::vector<std::shared_ptr<Max>> l4;
    std::shared_ptr<Sum> l5;
    std::shared_ptr<Divide> l6;

public:

    double forward(const std::vector<std::vector<double>> &X)
    {
        samples_num = X.size();

        for(int i = 0; i < X.size(); i++)
        {
            std::vector<std::shared_ptr<Square>> buf;
            for(int j = 0; j < X[0].size(); j++)
                buf.push_back(std::shared_ptr<Square>(new Square()));
            l1.push_back(buf);
        }
        for(int i = 0; i < X.size(); i++)
        {
            std::vector<std::shared_ptr<Sum>> buf;
            for(int j = 0; j < X[0].size() / 2; j++)
                buf.push_back(std::shared_ptr<Sum>(new Sum()));
            l2.push_back(buf);
        }
        for(int i = 0; i < X.size(); i++)
        {
            std::vector<std::shared_ptr<Sqrt>> buf;
            for(int j = 0; j < X[0].size() / 2; j++)
                buf.push_back(std::shared_ptr<Sqrt>(new Sqrt()));
            l3.push_back(buf);
        }
        for(int i = 0; i < X.size(); i++)
            l4.push_back(std::shared_ptr<Max>(new Max()));
        l5 = std::shared_ptr<Sum>(new Sum());
        l6 = std::shared_ptr<Divide>(new Divide());

        std::vector<std::vector<double>> _X;
        for(int i = 0; i < X.size(); i++)
        {
            std::vector<double> _x;
            for(int j = 0, k = 0; j < X[0].size(); j +=2, k++)
            {
                _x.push_back(
                        l3[i][k]->forward(l2[i][k]->forward(std::vector<double>{l1[i][j]->forward(X[i][j]), 
                                                                                l1[i][j+1]->forward(X[i][j+1])})));
            }
            _X.push_back(_x);
        }

        std::vector<double> __x;
        for(int i = 0; i < _X.size(); i++)
        {
            __x.push_back( l4[i]->forward(_X[i]) );
        }


        return l6->forward(l5->forward(__x), samples_num);
    }

    std::vector<std::vector<double>> backward(double d_out = 1)
    {
        auto out = l5->backward(l6->backward(d_out)[0]);

        std::vector<std::vector<double>> out1;
        for(int i = 0; i < l4.size(); i++)
        {
            out1.push_back(l4[i]->backward(out[i]));
        }

        std::vector<std::vector<double>> out2;
        for(int i = 0; i < l3.size(); i++)
        {
            std::vector<double> buf;
            for(int j = 0; j < l3[i].size(); j++)
                buf.push_back(l3[i][j]->backward(out1[i][j]));
            out2.push_back(buf);
        }

        std::vector<std::vector<double>> out3;
        for(int i = 0; i < l2.size(); i++)
        {
            std::vector<double> buf;
            for(int j = 0; j < l2[i].size(); j++)
            {
                auto _buf = l2[i][j]->backward(out2[i][j]);
                buf.insert(buf.end(), _buf.begin(), _buf.end());
            }
            out3.push_back(buf);
        }

        std::vector<std::vector<double>> out4;
        for(int i = 0; i < l1.size(); i++)
        {
            std::vector<double> buf;
            for(int j = 0; j < l1[i].size(); j++)
            {
                buf.push_back(l1[i][j]->backward(out3[i][j]));
            }
            out4.push_back(buf);
        }

        return out4;
    }

};


};


#endif