#ifndef OPTIM_H
#define OPTIM_H

#include <iostream>
#include "mat.h"
#include "dataset.h"


class Optimizer
{

public:

    virtual Mat::mat update(const Mat::mat &W, const Mat::mat &d_W, double learning_rate) = 0;

};


class SGD : public Optimizer
{

public:

    Mat::mat update(const Mat::mat &W, const Mat::mat &d_W, double learning_rate)
    {
        return Mat::diff(W, Mat::prod(d_W, learning_rate));
    }

};


class MomentumSGD : public Optimizer
{

private:

    double momentum;
    Mat::mat velocity;
    bool is_update;

public:

    MomentumSGD(double momentum = 0.9)
    {
        this->momentum = momentum;
        is_update = false;
    }

    Mat::mat update(const Mat::mat &W, const Mat::mat &d_W, double learning_rate)
    {
        if(!is_update)
        {
            velocity = Mat::zeros(W.size(), W[0].size());
            is_update = true;
        }
        velocity = Mat::diff(Mat::prod(velocity, momentum), Mat::prod(d_W, learning_rate));
        return Mat::sum(velocity, W);
    }

};



class Optimizer2d
{

public:

    virtual Mat2d::mat2d update(const Mat2d::mat2d &W, const Mat2d::mat2d &d_W, double learning_rate) = 0;

};


class SGD2d : public Optimizer2d
{

public:

    Mat2d::mat2d update(const Mat2d::mat2d &W, const Mat2d::mat2d &d_W, double learning_rate)
    {
        return Mat2d::diff(W, Mat2d::prod(d_W, learning_rate));
    }

};


#endif