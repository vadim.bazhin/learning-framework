#ifndef FILTER_H
#define FILTER_H

#include <iostream>
#include <vector>
#include "utils.h"
#include "mat.h"
#include "fft.h"
#include <thread>


namespace Filter
{

    Mat::mat Meadian(const Mat::mat &mat)
    {
        
        Mat::mat result(1, Vec::vec(mat[0].size(), 0));
        for(int i = 1; i < mat.size() - 1; i++)
        {
            Vec::vec _result;
            _result.push_back(0);
            for(int j = 1; j < mat[0].size() - 1; j++)
            {
                Vec::vec buffer;
                for(int k = i - 1; k <= i + 1; k++)
                    for(int l = j - 1; l <= j + 1; l++)
                        buffer.push_back(mat[k][l]);
                std::sort(buffer.begin(), buffer.end());
                _result.push_back(buffer[4]);
            }
            _result.push_back(0);
            result.push_back(_result);
        }
        result.push_back(Vec::vec(mat[0].size(), 0));

        return result;
    }

    Mat::mat Mean(const Mat::mat &mat)
    {
        
        Mat::mat result(1, Vec::vec(mat[0].size(), 0));
        for(int i = 1; i < mat.size() - 1; i++)
        {
            Vec::vec _result;
            _result.push_back(0);
            for(int j = 1; j < mat[0].size() - 1; j++)
            {
                double sum = 0;
                for(int k = i - 1; k <= i + 1; k++)
                    for(int l = j - 1; l <= j + 1; l++)
                        sum += mat[k][l];
                _result.push_back(int(sum / 9));
            }
            _result.push_back(0);
            result.push_back(_result);
        }
        result.push_back(Vec::vec(mat[0].size(), 0));

        return result;
    }

    Mat::mat Padding(const Mat::mat &mat, int padding_size, std::string type = "center")
    {
        if(type == "center")
        {
            Mat::mat result = Mat::zeros(mat.size() + (padding_size * 2), mat[0].size() + (padding_size * 2));
            for(int i = 0; i < mat.size(); i++)
                for(int j = 0; j < mat[0].size(); j++)
                    result[i+padding_size][j+padding_size] = mat[i][j];
            return result;
        }
        else if(type == "default")
        {
            Mat::mat result = Mat::zeros(mat.size() + padding_size, mat[0].size() + padding_size);
            for(int i = 0; i < mat.size(); i++)
                for(int j = 0; j < mat[0].size(); j++)
                    result[i][j] = mat[i][j];
            return result;
        }
        else
            throw std::invalid_argument("");
    }

    Mat::mat Gaussian(const Mat::mat &mat, int kernel = 3, double sigma = 1.0)
    {

        int scope = (kernel - 1) / 2;
        Mat::mat filter(Mat::zeros(kernel, kernel));

        double sum = 0.0;
        for(int i = -scope; i <= scope; i++)
        {
            for (int j = -scope; j <= scope; j++)
            {
                double s = 2 * sigma * sigma;
                filter[i+scope][j+scope] = std::exp(-(i*i + j*j) / s) / (Utils::PI * s);
                sum += filter[i+scope][j+scope];
            }
        }

        for (int i = 0; i < filter.size(); ++i)
            for (int j = 0; j < filter[0].size(); ++j)
                filter[i][j] /= sum;

        Mat::mat _mat = Filter::Padding(mat, scope);

        int h = _mat.size();
        int w = _mat[0].size();
        Mat::mat result(Mat::zeros(mat.size(), mat[0].size()));

        for(int i = scope; i < h - scope; i++)
        {
            for(int j = scope; j < w - scope; j++)
            {
                for(int k = i - scope; k <= i + scope; k++)
                    for(int l = j - scope; l <= j + scope; l++)
                        result[i-scope][j-scope] += filter[k-i+scope][l-j+scope] * _mat[k][l];
            }
        }

        return result;
    }

    Mat::mat Sobel(const Mat::mat &mat)
    {
        int h = mat.size();
        int w = mat[0].size();

        Mat::mat horizontal
        {
            {-1, 0, 1},
            {-2, 0, 2},
            {-1, 0, 1}
        };
        Mat::mat vertical
        {
            {-1, -2, -1},
            { 0,  0,  0},
            { 1,  2,  1}
        };

        Mat::mat new_mat(Mat::zeros(h, w));
        for(int i = 1; i < h - 1; i++)
        {
            for(int j = 1; j < w - 1; j++)
            {
                double horizontalGrad = 0;
                double verticalGrad = 0;
                for(int k = i - 1; k <= i + 1; k++)
                {
                    for(int l = j - 1; l <= j + 1; l++)
                    {
                        horizontalGrad = horizontalGrad + (horizontal[k-i+1][l-j+1] * mat[k][l]);
                        verticalGrad = verticalGrad + (vertical[k-i+1][l-j+1] * mat[k][l]);
                    }
                }
                new_mat[i][j] = std::sqrt(std::pow(horizontalGrad, 2.0) + std::pow(verticalGrad, 2.0));
            }
        }

        return new_mat;
    }

    Mat::mat Prewitt(const Mat::mat &mat)
    {
        int h = mat.size();
        int w = mat[0].size();

        Mat::mat horizontal
        {
            {-1, 0, 1},
            {-1, 0, 1},
            {-1, 0, 1}
        };
        Mat::mat vertical
        {
            {-1, -1, -1},
            { 0,  0,  0},
            { 1,  1,  1}
        };

        Mat::mat new_mat(Mat::zeros(h, w));
        for(int i = 1; i < h - 1; i++)
        {
            for(int j = 1; j < w - 1; j++)
            {
                double horizontalGrad = 0;
                double verticalGrad = 0;
                for(int k = i - 1; k <= i + 1; k++)
                {
                    for(int l = j - 1; l <= j + 1; l++)
                    {
                        horizontalGrad = horizontalGrad + (horizontal[k-i+1][l-j+1] * mat[k][l]);
                        verticalGrad = verticalGrad + (vertical[k-i+1][l-j+1] * mat[k][l]);
                    }
                }
                new_mat[i][j] = std::sqrt(std::pow(horizontalGrad, 2.0) + std::pow(verticalGrad, 2.0));
            }
        }

        return new_mat;
    }

    Mat::mat Threshold(const Mat::mat &mat, double threshold)
    {
        Mat::mat result(mat);
        for(int i = 0; i < mat.size(); i++)
            for(int j = 0; j < mat[0].size(); j++)
                if(result[i][j] < threshold)
                    result[i][j] = 0;
        return result;
    }

    Mat::mat MinMaxScale(const Mat::mat &mat, double min, double max)
    {
        Mat::mat result(mat);
        double old_min = Mat::min(result);
        double old_max = Mat::max(result);
        for(int i = 0; i < result.size(); i++)
            for(int j = 0; j < result.size(); j++)
                result[i][j] = Utils::rescaling(result[i][j], old_min, old_max, min, max);
        return result;
    }

    Mat::mat MaxPool(const Mat::mat &mat, int pool_size)
    {
        Mat::mat result;
        for(int i = 0; i < mat.size(); i+=pool_size)
        {
            Vec::vec buf;
            for(int j = 0; j < mat[0].size(); j+=pool_size)
            {
                double max = 0;
                for(int k = i; k < i+pool_size; k++)
                    for(int l = j; l < j+pool_size; l++)
                        if(mat[k][l] > max)
                            max = mat[k][l];
                buf.push_back(max);
            }
            result.push_back(buf);
        }
        return result;
    }

    Mat::mat Convolution(const Mat::mat &img, const Mat::mat &kernel)
    {
        if(img.size() != img[0].size())
            throw std::invalid_argument("");
        if(kernel.size() != kernel[0].size())
            throw std::invalid_argument("");

        int filter_size = kernel.size();
        int img_size = img.size();
        int out_N = (img.size() - kernel.size()) + 1;
        Mat::mat result = Mat::zeros(out_N, out_N);
        for(int h_out = 0; h_out < out_N; h_out++)
            for(int w_out = 0; w_out < out_N; w_out++)
            {
                Mat::mat img_slice;
                for(int h = h_out, i = 0; h < h_out+filter_size; h++, i++)
                {
                    img_slice.push_back(Vec::vec());
                    for(int w = w_out; w < w_out+filter_size; w++)
                        img_slice[i].push_back(img[h][w]);
                }
                result[h_out][w_out] = Mat::sum(Mat::prod(img_slice, kernel));
            }
        return result;
    }

    Mat::mat ConvolutionFFT(const Mat::mat &img, const Mat::mat &kernel)
    {
        if(img.size() != img[0].size())
            throw std::invalid_argument("");
        if(kernel.size() != kernel[0].size())
            throw std::invalid_argument("");

        int N = img.size() + kernel.size() - 1;
        int n = std::pow(2, std::ceil(std::log2(N)));
        int out_N = (img.size() - kernel.size()) + 1;
        Mat::mat result = Mat::zeros(out_N, out_N);

        fft::w_type** complex_x = (fft::w_type**)malloc(n * sizeof(fft::w_type*));
        for(int i = 0; i < n; i++)
        {
            complex_x[i] = (fft::w_type*)malloc(n * sizeof(fft::w_type));
            for (int j = 0; j < n; j++)
            {
                if(i >= img.size() || j >= img[0].size())
                    complex_x[i][j].r = 0;
                else
                    complex_x[i][j].r = img[i][j];
                complex_x[i][j].i = 0;
            }
        }

        Mat::mat kernel_reverse = Mat::zeros(kernel.size(), kernel.size());
        for(int i = 0, _i = kernel.size() - 1; i < kernel.size(); i++, _i--)
            for (int j = 0, _j = kernel.size() - 1; j < kernel.size(); j++, _j--)
                kernel_reverse[i][j] = kernel[_i][_j];

        fft::w_type** complex_y = (fft::w_type**)malloc(n * sizeof(fft::w_type*));
        for(int i = 0; i < n; i++)
        {
            complex_y[i] = (fft::w_type*)malloc(n * sizeof(fft::w_type));
            for (int j = 0; j < n; j++)
            {
                if(i >= kernel_reverse.size() || j >= kernel_reverse[0].size())
                    complex_y[i][j].r = 0;
                else
                    complex_y[i][j].r = kernel_reverse[i][j];
                complex_y[i][j].i = 0;
            }
        }

        fft::fft2(complex_x, n);
        fft::fft2(complex_y, n);
        fft::prod(complex_x, complex_y, n);
        fft::ifft2(complex_x, n);

        int indent = (n - out_N) / 2;
        for(int i = 0; i < out_N; i++)
            for (int j = 0; j < out_N; j++)
                result[i][j] = complex_x[i+indent][j+indent].r;
        result = Mat::div(result, n*n);


        for(int i = 0; i < n; i++)
            free(complex_x[i]);
        free(complex_x);

        for(int i = 0; i < n; i++)
            free(complex_y[i]);
        free(complex_y);

        return result;
    }

};


#endif