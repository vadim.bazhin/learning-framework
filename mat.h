#ifndef MAT_H
#define MAT_H

#include <iostream>
#include <vector>
#include <cmath>
#include "rand.h"
#include <algorithm>


namespace Vec
{


typedef std::vector<double> vec;

vec sum(vec vec1, vec vec2)
{
    if(vec1.size() != vec2.size())
        throw std::invalid_argument("");

    vec result;
    for(int i = 0; i < vec1.size(); i++)
        result.push_back(vec1[i] + vec2[i]);

    return result;
}

vec zeros(int count)
{
    return vec(count, 0);
}

};

namespace Mat
{


typedef std::vector<Vec::vec> mat;


mat init(int rows, int columns, double value)
{
    return mat(rows, Vec::vec(columns, value));
}


mat ones(int rows, int columns)
{
    return init(rows, columns, 1);
}


mat zeros(int rows, int columns)
{
    return init(rows, columns, 0);
}


mat randn(int rows, int columns)
{
    mat result(rows);
    for(int i = 0; i < rows; i++)
        for(int j = 0; j < columns; j++)
            result[i].push_back(Random::random_double(-1, 1));
    return result;
}


mat dot(const mat &mat1, const mat &mat2)
{
    if(mat1.size() == 0 || mat1[0].size() == 0)
        throw std::invalid_argument("");
    if(mat2.size() == 0 || mat2[0].size() == 0)
        throw std::invalid_argument("");
    if(mat1[0].size() != mat2.size())
        throw std::invalid_argument("");

    mat result = zeros(mat1.size(), mat2[0].size());
    for(int i = 0; i < mat1.size(); i++)
        for(int j = 0; j < mat2[0].size(); j++)
            for(int k = 0; k < mat2.size(); k++)
                result[i][j] += mat1[i][k] * mat2[k][j];
    return result;
}


void print(const mat &_mat)
{
    for(int i = 0; i < _mat.size(); i++)
    {
        for(int j = 0; j < _mat[i].size(); j++)
            std::cout << _mat[i][j] << " ";
        std::cout << std::endl;
    }
}


mat sum(const mat &mat1, const mat &mat2)
{
    if(mat1.size() == 0 || mat1[0].size() == 0)
        throw std::invalid_argument("");
    if(mat2.size() == 0 || mat2[0].size() == 0)
        throw std::invalid_argument("");

    mat result(mat1);

    if(mat2.size() == 1)
    {
        if(mat1[0].size() != mat2[0].size())
            throw std::invalid_argument("");

        for(int i = 0; i < result.size(); i++)
            for(int j = 0; j < result[i].size(); j++)
                result[i][j] += mat2[0][j];
    }
    else
    {
        if(mat1.size() != mat2.size())
            throw std::invalid_argument("");
        if(mat1[0].size() != mat2[0].size())
            throw std::invalid_argument("");

        for(int i = 0; i < result.size(); i++)
            for(int j = 0; j < result[i].size(); j++)
                result[i][j] += mat2[i][j];
    }
    return result;
}


double sum(const mat &_mat)
{
    if(_mat.size() == 0 || _mat[0].size() == 0)
        throw std::invalid_argument("");

    double result = 0;
    for(int i = 0; i < _mat.size(); i++)
        for(int j = 0; j < _mat[i].size(); j++)
            result += _mat[i][j];
    return result;
}


mat diff(const mat &mat1, const mat &mat2)
{
    if(mat1.size() == 0 || mat1[0].size() == 0)
        throw std::invalid_argument("");
    if(mat2.size() == 0 || mat2[0].size() == 0)
        throw std::invalid_argument("");
    if(mat1.size() != mat2.size())
        throw std::invalid_argument("");
    if(mat1[0].size() != mat2[0].size())
        throw std::invalid_argument("");

    mat result(mat1);
    for(int i = 0; i < result.size(); i++)
        for(int j = 0; j < result[i].size(); j++)
            result[i][j] -= mat2[i][j];
    return result;
}


mat pow(const mat &_mat, double power)
{
    if(_mat.size() == 0 || _mat[0].size() == 0)
        throw std::invalid_argument("");

    mat result(_mat);
    for(int i = 0; i < result.size(); i++)
        for(int j = 0; j < result[i].size(); j++)
            result[i][j] = std::pow(result[i][j], power);
    return result;
}


mat prod(const mat &mat1, const mat &mat2)
{
    if(mat1.size() == 0 || mat1[0].size() == 0)
        throw std::invalid_argument("");
    if(mat2.size() == 0 || mat2[0].size() == 0)
        throw std::invalid_argument("");
    if(mat1.size() != mat2.size())
        throw std::invalid_argument("");
    if(mat1[0].size() != mat2[0].size())
        throw std::invalid_argument("");

    mat result(mat1);
    for(int i = 0; i < result.size(); i++)
        for(int j = 0; j < result[i].size(); j++)
            result[i][j] *= mat2[i][j];
    return result;
}


mat prod(const mat &_mat, double value)
{
    if(_mat.size() == 0 || _mat[0].size() == 0)
        throw std::invalid_argument("");

    mat result(_mat);
    for(int i = 0; i < result.size(); i++)
        for(int j = 0; j < result[i].size(); j++)
            result[i][j] *= value;
    return result;
}


mat div(const mat &_mat, double value)
{
    if(_mat.size() == 0 || _mat[0].size() == 0)
        throw std::invalid_argument("");
    if(value == 0)
        throw std::invalid_argument("");

    mat result(_mat);
    for(int i = 0; i < result.size(); i++)
        for(int j = 0; j < result[i].size(); j++)
            result[i][j] /= value;
    return result;
}


mat abs(const mat &_mat)
{
    if(_mat.size() == 0 || _mat[0].size() == 0)
        throw std::invalid_argument("");

    mat result(_mat);
    for(int i = 0; i < _mat.size(); i++)
        for(int j = 0; j < _mat[i].size(); j++)
            if(result[i][j] < 0)
                result[i][j] = result[i][j] * -1;
    return result;
}


mat transpose(const mat &_mat)
{
    if(_mat.size() == 0 || _mat[0].size() == 0)
        throw std::invalid_argument("");

    mat result = zeros(_mat[0].size(), _mat.size());
    for(int i = 0; i < _mat[0].size(); i++)
        for(int j = 0; j < _mat.size(); j++)
            result[i][j] = _mat[j][i];
    return result;
}


double mean(const mat &_mat)
{
    if(_mat.size() == 0)
        throw std::invalid_argument("");

    return (double)sum(_mat) / (double)(_mat.size() * _mat[0].size());
}


double var(const mat &_mat)
{
    if(_mat.size() == 0)
        throw std::invalid_argument("");

    double _mean = mean(_mat);
    double result = 0;
    for(int i = 0; i < _mat.size(); i++)
        for(int j = 0; j < _mat[i].size(); j++)
            result += std::pow(_mat[i][j] - _mean, 2);
    return (double)result / (double)(_mat.size() * _mat[0].size());
}


double std(const mat &_mat)
{
    if(_mat.size() == 0)
        throw std::invalid_argument("");

    return std::sqrt(var(_mat));
}


double min(const mat &_mat)
{
    double min = _mat[0][0];
    for(int i = 0; i < _mat.size(); i++)
        for(int j = 0; j < _mat[0].size(); j++)
            if(_mat[i][j] < min)
                min = _mat[i][j];
    return min;
}


double max(const mat &_mat)
{
    double max = _mat[0][0];
    for(int i = 0; i < _mat.size(); i++)
        for(int j = 0; j < _mat[0].size(); j++)
            if(_mat[i][j] > max)
                max = _mat[i][j];
    return max;
}


};


namespace Mat2d
{


typedef std::vector<std::vector<Mat::mat>> mat2d;


class size_t
{

private:

    std::size_t size[4];

public:

    size_t(){}

    size_t(std::size_t d1, std::size_t d2, std::size_t d3, std::size_t d4)
        {
            size[0] = d1;
            size[1] = d2; 
            size[2] = d3; 
            size[3] = d4;
        }

    std::size_t operator[] (std::size_t item)
    {
        return size[item];
    }

};


size_t size(const mat2d &mat)
{
    return size_t(mat.size(), 
                  mat[0].size(), 
                  mat[0][0].size(), 
                  mat[0][0][0].size());
}

size_t size(mat2d &mat)
{
    return size_t(mat.size(), 
                  mat[0].size(), 
                  mat[0][0].size(), 
                  mat[0][0][0].size());
}


mat2d init(size_t size, double value)
{
    return mat2d(size[0], std::vector<Mat::mat>
                (size[1], Mat::mat
                (size[2], Vec::vec
                (size[3], value))));
}

mat2d init(size_t size)
{
    return mat2d(size[0], std::vector<Mat::mat>
                (size[1], Mat::mat
                (size[2], Vec::vec
                (size[3]))));
}


mat2d zeros(size_t size)
{
    return init(size, 0.0);
}


mat2d ones(size_t size)
{
    return init(size, 1.0);
}


mat2d randn(size_t size)
{
    mat2d result = zeros(size);
    for(int i = 0; i < size[0]; i++)
        for(int j = 0; j < size[1]; j++)
            for(int k = 0; k < size[2]; k++)
                for(int l = 0; l < size[3]; l++)
                    result[i][j][k][l] = Random::random_double(-1.0, 1.0);
    return result;
}


void print(const mat2d &mat)
{
    for(int i = 0; i < size(mat)[0]; i++)
    {
        for(int j = 0; j < size(mat)[1]; j++)
        {
            Mat::print(mat[i][j]);
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }
}


mat2d diff(const mat2d &mat1, const mat2d &mat2)
{
    Mat2d::size_t mat1_size = size(mat1);
    Mat2d::size_t mat2_size = size(mat2);

    if(mat1_size[0] != mat2_size[0] || mat1_size[1] != mat2_size[1] ||
       mat1_size[2] != mat2_size[2] || mat1_size[3] != mat2_size[3])
        throw std::invalid_argument("");
    if(mat1.empty() || mat2.empty())
        throw std::invalid_argument("");

    int batch_size = mat1_size[0];
    int channels = mat1_size[1];
    int height = mat1_size[2];
    int width = mat1_size[3];

    mat2d result = mat2d(mat1);
    for(int b = 0; b < batch_size; b++)
        for(int c = 0; c < channels; c++)
            for(int h = 0; h < height; h++)
                for(int w = 0; w < width; w++)
                    result[b][c][h][w] -= mat2[b][c][h][w];

    return result;
}


mat2d pow(const mat2d &_mat, double power)
{
    if(_mat.empty())
        throw std::invalid_argument("");

    Mat2d::size_t mat_size = size(_mat);
    int batch_size = mat_size[0];
    int channels = mat_size[1];
    int height = mat_size[2];
    int width = mat_size[3];

    mat2d result = zeros(mat_size);
    for(int b = 0; b < batch_size; b++)
        for(int c = 0; c < channels; c++)
            for(int h = 0; h < height; h++)
                for(int w = 0; w < width; w++)
                    result[b][c][h][w] = std::pow(_mat[b][c][h][w], power);

    return result;
}


double sum(const mat2d &_mat)
{
    if(_mat.empty())
        throw std::invalid_argument("");
    
    Mat2d::size_t mat_size = size(_mat);
    int batch_size = mat_size[0];
    int channels = mat_size[1];
    int height = mat_size[2];
    int width = mat_size[3];

    double result = 0;
    for(int b = 0; b < batch_size; b++)
        for(int c = 0; c < channels; c++)
            for(int h = 0; h < height; h++)
                for(int w = 0; w < width; w++)
                    result += _mat[b][c][h][w];

    return result;
}


mat2d sum(const mat2d &mat1, const mat2d &mat2)
{
    Mat2d::size_t mat1_size = size(mat1);
    Mat2d::size_t mat2_size = size(mat2);

    if(mat1_size[0] != mat2_size[0] || mat1_size[1] != mat2_size[1] ||
       mat1_size[2] != mat2_size[2] || mat1_size[3] != mat2_size[3])
        throw std::invalid_argument("");
    if(mat1.empty() || mat2.empty())
        throw std::invalid_argument("");

    int batch_size = mat1_size[0];
    int channels = mat1_size[1];
    int height = mat1_size[2];
    int width = mat1_size[3];

    mat2d result = mat2d(mat1);
    for(int b = 0; b < batch_size; b++)
        for(int c = 0; c < channels; c++)
            for(int h = 0; h < height; h++)
                for(int w = 0; w < width; w++)
                    result[b][c][h][w] += mat2[b][c][h][w];

    return result;
}


mat2d prod(const mat2d &_mat, double value)
{
    if(_mat.empty())
        throw std::invalid_argument("");

    Mat2d::size_t mat_size = size(_mat);
    int batch_size = mat_size[0];
    int channels = mat_size[1];
    int height = mat_size[2];
    int width = mat_size[3];

    mat2d result = mat2d(_mat);
    for(int b = 0; b < batch_size; b++)
        for(int c = 0; c < channels; c++)
            for(int h = 0; h < height; h++)
                for(int w = 0; w < width; w++)
                    result[b][c][h][w] *= value;

    return result;
}


};


#endif