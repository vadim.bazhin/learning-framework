#ifndef DATASET_H
#define DATASET_H

#include <iostream>
#include "mat.h"


class StandartScaling
{

private:

    double std = 0;
    double mean = 0;

public:

    StandartScaling(const Mat::mat &X)
    {
        mean = Mat::mean(X);
        std = Mat::std(X);
    }

    StandartScaling(double std, double mean)
    {
        this->std = std;
        this->mean = mean;
    }

    Mat::mat transform(const Mat::mat &X)
    {
        Mat::mat z(X);
        for(int i = 0; i < z.size(); i++)
            for(int j = 0; j < z[i].size(); j++)
                z[i][j] = (z[i][j] - mean) / std;
        return z;
    }

    Mat::mat inverse_transform(const Mat::mat &X)
    {
        Mat::mat result(X);
        for(int i = 0; i < result.size(); i++)
            for(int j = 0; j < result[i].size(); j++)
                result[i][j] = (result[i][j] * std) + mean;
        return result;
    }

};


class Dataset
{

public:

    Mat::mat train_X;
    Mat::mat train_y;
    Mat::mat val_X;
    Mat::mat val_y;

    Dataset(const Mat::mat &train_X, const Mat::mat &train_y,
            const Mat::mat &val_X, const Mat::mat &val_y):
        train_X(train_X),
        train_y(train_y),
        val_X(val_X),
        val_y(val_y)
        {}

    Mat::mat get_batch_x(const std::vector<int> &indices)
    {
        Mat::mat batch_x;
        for(int i = 0; i < indices.size(); i++)
            batch_x.push_back(train_X[indices[i]]);
        return batch_x;
    }

    Mat::mat get_batch_y(const std::vector<int> &indices)
    {
        Mat::mat batch_y;
        for(int i = 0; i < indices.size(); i++)
            batch_y.push_back(train_y[indices[i]]);
        return batch_y;
    }

};


class Dataset2d
{

public:

    Mat2d::mat2d train_X;
    Mat2d::mat2d train_y;
    Mat2d::mat2d val_X;
    Mat2d::mat2d val_y;

    Dataset2d(const Mat2d::mat2d &train_X, const Mat2d::mat2d &train_y,
              const Mat2d::mat2d &val_X, const Mat2d::mat2d &val_y):
        train_X(train_X),
        train_y(train_y),
        val_X(val_X),
        val_y(val_y)
        {}

    Mat2d::mat2d get_batch_x(const std::vector<int> &indices)
    {
        Mat2d::mat2d batch_x;
        for(int i = 0; i < indices.size(); i++)
            batch_x.push_back(train_X[indices[i]]);
        return batch_x;
    }

    Mat2d::mat2d get_batch_y(const std::vector<int> &indices)
    {
        Mat2d::mat2d batch_y;
        for(int i = 0; i < indices.size(); i++)
            batch_y.push_back(train_y[indices[i]]);
        return batch_y;
    }

};


#endif