#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include "generate.h"
#include "layers.h"
#include "mat.h"
#include "models.h"
#include "optim.h"
#include "trainer.h"
#include "dataset.h"
#include "computational_graph.h"
#include "filter.h"
// #include <complex>
#include "fft.h"
// #include <thread>


/* FOR SOLVER OLD */
/*
std::shared_ptr<Dataset> load_triangle_dataset()
{
    std::vector<std::string> data_dirs{
                                       "data/0.0/",
                                       "data/0.1/",
                                       "data/0.2/",
                                       "data/0.3/",
                                       "data/0.4/",
                                       "data/0.5/",
                                       "data/0.6/",
                                       "data/0.7/",
                                       "data/0.8/",
                                       "data/0.9/"
                                      };
    std::vector<std::string> file_names{"00.pgm",
                                        "01.pgm",
                                        "02.pgm",
                                        "03.pgm",
                                        "04.pgm",
                                        "05.pgm",
                                        "06.pgm",
                                        "07.pgm",
                                        "08.pgm",
                                        "09.pgm"};
    std::string gt = "ans.txt";

    std::vector<std::vector<double>> X;
    std::vector<std::vector<double>> y;
    for(int i = 0; i < data_dirs.size(); i++)
    {
        for(int j = 0; j < file_names.size(); j++)
        {
            std::ifstream is(data_dirs[i] + file_names[j]);
            std::string buffer;
            std::vector<std::string> data;
            while (std::getline(is, buffer))
                if(buffer.size() > 0)
                    data.push_back(buffer);

            std::vector<double> img;
            std::istringstream s(data[3]);
            double d;
            while (s >> d)
                img.push_back(d);

            X.push_back(img);
        }

        std::ifstream is(data_dirs[i] + gt);
        std::string buffer;
        std::vector<std::string> data;
        while (std::getline(is, buffer))
            if(buffer.size() > 0)
                data.push_back(buffer);

        for(int j = 0; j < data.size(); j++)
        {
            std::vector<double> points;
            std::istringstream s(data[j]);
            double d;
            while (s >> d)
                points.push_back(d);
            y.push_back(points);
        }
    }

    StandartScaling stand_X = StandartScaling(X);
    return std::shared_ptr<Dataset>(new Dataset(stand_X.transform(X), y, stand_X.transform(X), y));
}
*/

std::vector<Point> meanshift(std::vector<Point> points, int epochs=150, double max_dist = 35.0)
{
    std::vector<Point> clusters = points;
    std::vector<Point> clusters_hist;
    std::vector<bool> convergence(clusters.size(), false);

    for(int epoch = 0; epoch < epochs; epoch++)
    {
        clusters_hist = clusters;
        for(int c = 0; c < clusters.size(); c++)
        {
            if(convergence[c])
                continue;

            std::vector<double> distances;
            for(int p = 0; p < points.size(); p++)
                distances.push_back(clusters[c].distance(points[p]));

            Point new_centr(0.0, 0.0);
            int count = 0;
            for(int p = 0; p < points.size(); p++)
            {
                if(distances[p] > max_dist)
                    continue;

                new_centr.X += points[p].X;
                new_centr.Y += points[p].Y;
                count++;
            }

            new_centr.X /= count;
            new_centr.Y /= count;

            clusters[c].X = new_centr.X;
            clusters[c].Y = new_centr.Y;

            if(clusters[c].X == clusters_hist[c].X && clusters[c].Y == clusters_hist[c].Y)
                convergence[c] = true;
        }

        int count = 0;
        for(int c = 0; c < clusters.size(); c++)
        {
            if(convergence[c])
                count++;
        }
        if(clusters.size() == count)
            break;
    }

    std::vector<Point> unique_clusters;
    std::vector<double> uses(clusters.size(), false);
    for(int c = 0; c < clusters.size(); c++)
    {
        if(uses[c])
            continue;
        unique_clusters.push_back(clusters[c]);
        uses[c] = true;
        for(int i = c+1; i < clusters.size(); i++)
            if(clusters[i].X == clusters[c].X && clusters[i].Y == clusters[c].Y)
                uses[i] = true;
    }

    return unique_clusters;
}

std::vector<Point> kmeans(std::vector<Point> points, int epochs=200, int k=3)
{
    Triangle triangle = TriangleGenerator::generate(1, 498);

    std::vector<Point> centroids{triangle[0], triangle[1], triangle[2]};

    std::vector<Point> centroids_hist;

    for(int epoch = 0; epoch < epochs; epoch++)
    {
        centroids_hist = centroids;

        std::vector<size_t> idx(points.size());
        std::vector<size_t> centroids_count(centroids.size(), 0);
        for(int i = 0; i < points.size(); i++)
        {
            double min = Utils::double_max;
            size_t cluster_idx;
            for(int j = 0; j < k; j++)
            {
                double dist = centroids[j].distance(points[i]);
                if(dist < min)
                {
                    min = dist;
                    cluster_idx = j;
                }
            }
            idx[i] = cluster_idx;
            centroids_count[cluster_idx]++;
        }

        bool flag = false;
        for(int j = 0; j < k; j++)
        {
            if (centroids_count[j] == 0)
            {
                Triangle triangle = TriangleGenerator::generate(1, 498);

                centroids[0] = triangle[0];
                centroids[1] = triangle[1];
                centroids[2] = triangle[2];
                flag = true;
            }
        }

        if(flag)
        {
            // std::cout << "fail\n";
            continue;
        }

        for(int j = 0; j < k; j++)
        {
            centroids[j].X = 0.0;
            centroids[j].Y = 0.0;
        }

        for(int i = 0; i < points.size(); i++)
        {
            centroids[idx[i]].X += points[i].X;
            centroids[idx[i]].Y += points[i].Y;
        }

        for(int j = 0; j < k; j++)
        {
            centroids[j].X /= centroids_count[j];
            centroids[j].Y /= centroids_count[j];
        }

        size_t counter = 0;
        for(int j = 0; j < k; j++)
            if(centroids[j].X == centroids_hist[j].X && centroids[j].Y == centroids_hist[j].Y)
                counter++;
        if(counter == k)
        {
            // std::cout << "epoch: " << epoch << "\n";

            Triangle test = Triangle(centroids[0], centroids[1], centroids[2]);
            auto _dist = test.getDistances();
            auto _angle = test.getAngles();
            if (_dist[0] < 90 || _dist[1] < 90 || _dist[2] < 90)
            {
                Triangle triangle = TriangleGenerator::generate(1, 498);

                centroids[0] = triangle[0];
                centroids[1] = triangle[1];
                centroids[2] = triangle[2];
                continue;
            }
            if (_angle[0] < 27 || _angle[1] < 27 || _angle[2] < 27)
            {
                Triangle triangle = TriangleGenerator::generate(1, 498);

                centroids[0] = triangle[0];
                centroids[1] = triangle[1];
                centroids[2] = triangle[2];
                continue;
            }

            break;
        }

    }

    return centroids;
}



/* MAIN TEST */
/*
int main(int argc, char* argv[])
{
    // std::cout << std::fixed;

    int test_size = 100;

    int fail_count = 0;
    for(int i = 0; i < test_size; i++)
    {
// auto begin = std::chrono::steady_clock::now();

        Mat2d::mat2d X;
        std::vector<Point> points_target;


        {   // generate
            Triangle triangle = TriangleGenerator::generate(1, 498);

            std::ofstream file("output.txt");
            file << triangle[0].X << " " << triangle[0].Y << std::endl
                 << triangle[1].X << " " << triangle[1].Y << std::endl
                 << triangle[2].X << " " << triangle[2].Y;
            points_target.push_back(Point(triangle[0].X, triangle[0].Y));
            points_target.push_back(Point(triangle[1].X, triangle[1].Y));
            points_target.push_back(Point(triangle[2].X, triangle[2].Y));
            file.close();

            PGM_P2_255 img = PGM_P2_255(500, 500);
            img.draw(triangle);
            img.draw_noise(0.6);
            Mat::mat img_mat = img.to_mat();
            // Mat::mat img_mat = Filter::Padding(img_mat, 30);
            for(int i = 0; i < img_mat.size(); i++)
                for(int j = 0; j < img_mat.size(); j++)
                    img_mat[i][j] = Utils::rescaling(img_mat[i][j], 0, 255, 0, 1);
            X.push_back(std::vector<Mat::mat>{img_mat});

        }


        // {   // public

        //     int p_dir = 5;
        //     int image_idx = i;
        //     std::string dir_name = "data";
        //     // std::string dir_name = "fail";

        //     std::string image_path = dir_name + "/0." + std::to_string(p_dir) + "/" + "0" + std::to_string(image_idx) + ".pgm";
        //     // std::string image_path = dir_name + "/0." + std::to_string(p_dir) + "/" + std::to_string(image_idx) + ".pgm";
        //     std::string ans_path = dir_name + "/0." + std::to_string(p_dir) + "/ans.txt";

        //     {
        //         std::ifstream is(image_path);
        //         std::string buffer;
        //         std::vector<std::string> data;
        //         while (std::getline(is, buffer))
        //             if(buffer.size() > 0)
        //                 data.push_back(buffer);

        //         Mat::mat img(500, Vec::vec(500));
        //         std::istringstream s(data[3]);
        //         double d;
        //         for(int i = 0; i < 500; i++)
        //             for(int j = 0; j < 500; j++)
        //                 if(s >> d)
        //                     img[i][j] = Utils::rescaling(d, 0, 255, 0, 1);

        //         X = Mat2d::init(Mat2d::size_t(1,1,500,500));
        //         X[0][0] = img;
        //     }
        //     {
        //         std::ifstream is(ans_path);
        //         std::string buffer;
        //         std::vector<std::string> data;
        //         while (std::getline(is, buffer))
        //             if(buffer.size() > 0)
        //                 data.push_back(buffer);

        //         std::vector<double> points;
        //         std::istringstream s(data[image_idx]);
        //         double d;
        //         while(s >> d)
        //             points.push_back(d);

        //         for(int i = 0; i < points.size(); i += 2)
        //             points_target.push_back(Point(points[i], points[i+1]));
        //     }
        // }


// std::cout << "[data generate]: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count() << " ms" << std::endl;


auto main_begin = std::chrono::steady_clock::now();

// begin = std::chrono::steady_clock::now();
        UNet_predict_only unet;
// std::cout << "[model init]: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count() << " ms" << std::endl;
    
// begin = std::chrono::steady_clock::now();
//         unet.load_from_pytorch();
// std::cout << "[model load]: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count() << " ms" << std::endl;

// begin = std::chrono::steady_clock::now();
//         unet.save();
// std::cout << "[model save]: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count() << " ms" << std::endl;

// begin = std::chrono::steady_clock::now();
        unet.load();
// std::cout << "[model load v2]: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count() << " ms" << std::endl;

// begin = std::chrono::steady_clock::now();
        Mat2d::mat2d pred = unet.predict(X);

// std::cout << "[model predict]: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count() << " ms" << std::endl;

        
        PGM_P2_255(Filter::MinMaxScale(X[0][0], 0, 255)).to_file("image.pgm");
        // PGM_P2_255(Filter::MinMaxScale(pred[0][0], 0, 255)).to_file("pred.pgm");


        Mat::mat pred_mat = Filter::MinMaxScale(pred[0][0], 0, 255);

        double threshold = 170;
        double threshold_step = 1;
        int end_shift = 3;
        int step_count = 200;
        std::vector<Point> points_pred;
        std::vector<std::vector<Point>> points_pred_hist;
        for(int i = 0; i < step_count; i++)
        {
            Mat::mat _pred_mat = Filter::Threshold(pred_mat, threshold);
            
            std::vector<Point> points;
            for(int i = 0; i < _pred_mat.size(); i++)
                for(int j = 0; j < _pred_mat[0].size(); j++)
                    if(_pred_mat[i][j] != 0)
                        points.push_back(Point(j, i));

            // std::vector<Point> points_pred = kmeans(points);
            // std::vector<Point> _points_pred = meanshift(points);
            // std::cout << "Point count: " << _points_pred.size() << "\n";
            // if(_points_pred.size() == 3)
            //     points_pred = _points_pred;

            points_pred = meanshift(points);
            if(points_pred.size() < 3)
            {
                if(points_pred_hist.size() != 0)
                {
                    // int pred_idx;
                    // if(points_pred_hist.size() < end_shift)
                    //     pred_idx = 0;
                    // else
                    //     pred_idx = points_pred_hist.size() - end_shift;

                    int pred_idx = ((points_pred_hist.size() - 1) / 3) * 2;

                    points_pred = points_pred_hist[pred_idx];
                    break;
                }
                threshold -= threshold_step;
            }
            else if(points_pred.size() == 3)
            {
                points_pred_hist.push_back(points_pred);
                pred_mat = _pred_mat;
                threshold += threshold_step;
            }
            else
            {
                threshold += threshold_step;
            }
            points_pred = std::vector<Point>{Point(0.0, 0.0), Point(0.0, 0.0), Point(0.0, 0.0)};
        }
        PGM_P2_255(pred_mat).to_file("pred.pgm");

std::cout << "[all]: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - main_begin).count() << " ms" << std::endl;

        // std::cout << "\npoints target:\n";
        // for(int i = 0; i < points_target.size(); i++)
        //     std::cout << points_target[i].X << " " << points_target[i].Y << "\n";
        // std::cout << "\npoints predict:\n";
        // for(int i = 0; i < points_pred.size(); i++)
        //     std::cout << points_pred[i].X << " " << points_pred[i].Y << "\n";

        std::vector<double> min_dists(points_target.size());
        std::vector<double> min_dists_idx(points_target.size());
        for(int i = 0; i < points_target.size(); i++)
        {
            double min_dist = Utils::double_max;
            int min_dist_idx;
            for(int j = 0; j < points_pred.size(); j++)
            {
                for(int k = 0; k < i; k++)
                    if(j == min_dists_idx[k])
                        continue;
                double dist = points_target[i].distance(points_pred[j]);
                if(min_dist > dist)
                {
                    min_dist = dist;
                    min_dist_idx = j;
                }
            }
            min_dists[i] = min_dist;
            min_dists_idx[i] = min_dist_idx;
        }
        double max_dist = 0;
        for(int i = 0; i < min_dists.size(); i++)
            if(max_dist < min_dists[i])
                max_dist = min_dists[i];
        std::cout << "[" << i << "] max distance: " << max_dist;
        if(max_dist >= 5.0)
            std::cout << " [fail]\n";
        else
            std::cout << "\n";

        // std::string fail_dir = "fail/0.5/";
        // if(max_dist >= 5.0)
        // {
        //     PGM_P2_255(Filter::MinMaxScale(X[0][0], 0, 255)).to_file(fail_dir + std::to_string(fail_count) + ".pgm");
        //     PGM_P2_255(pred_mat).to_file(fail_dir + std::to_string(fail_count) + "_pred.pgm");
        //     std::ofstream file(fail_dir + "ans.txt", std::ios::app);
        //     file << points_target[0].X << " " << points_target[0].Y << " "
        //          << points_target[1].X << " " << points_target[1].Y << " "
        //          << points_target[2].X << " " << points_target[2].Y << std::endl;
        //     file.close();

        //     fail_count++;
        // }



    }
    return 0;
}
*/

/* UNET TRAIN TEST */
/*
int main(int argc, char* argv[])
{
    std::cout << std::fixed;
    //UNet unet = UNet();

    Mat2d::mat2d X;
    Mat2d::mat2d y;
    for (int i = 0; i < 1; i++)
    {
        Triangle triangle = TriangleGenerator::generate(1, 498);

        PGM_P2_255 img = PGM_P2_255(500, 500);
        img.draw(triangle);
        //img.draw_noise(0.1);
        Mat::mat _x = Filter::Padding(img.to_mat(), 30);
        for(int i = 0; i < _x.size(); i++)
            for(int j = 0; j < _x.size(); j++)
                _x[i][j] = Utils::rescaling(_x[i][j], 0, 255, 0, 1);
        X.push_back(std::vector<Mat::mat>{_x});

        PGM_P2_255 mark = PGM_P2_255(500, 500);
        mark.iplot(Utils::round(triangle[0].getX()), Utils::round(triangle[0].getY()), 255);
        mark.iplot(Utils::round(triangle[1].getX()), Utils::round(triangle[1].getY()), 255);
        mark.iplot(Utils::round(triangle[2].getX()), Utils::round(triangle[2].getY()), 255);


        auto out = mark.to_mat();
        out = Filter::Padding(out, 30);
        //out = Filter::MaxPool(out, 10);
        out = Filter::Gaussian(out, 61, 12);
        out = Filter::MinMaxScale(out, 0, 255);
        Mat::mat _y = out;
        for(int i = 0; i < _y.size(); i++)
            for(int j = 0; j < _y.size(); j++)
                _y[i][j] = Utils::rescaling(_y[i][j], 0, 255, 0, 1);
        y.push_back(std::vector<Mat::mat>{_y});
    }


    int num_epochs = 5;
    int batch_size = 1;
    double learning_rate = 0.0000001;

    std::shared_ptr<Dataset2d> dataset = std::shared_ptr<Dataset2d>(new Dataset2d(X, y, Mat2d::mat2d(), Mat2d::mat2d()));
    UNetTrainer trainer(dataset, num_epochs, batch_size, learning_rate);
    
    std::cout << "Model fit ..." << std::endl;
    trainer.fit();

}
*/

/* DATA GENERATE */
/*
int main(int argc, char* argv[])
{
    std::cout << std::fixed;


    for (int i = 0; i < 100; i++)
    {
        Triangle triangle = TriangleGenerator::generate(1, 498);

        PGM_P2_255 img = PGM_P2_255(500, 500);
        img.draw(triangle);
        img.draw_noise(0.8);
        Mat::mat img_mat = img.to_mat();
        // img_mat = Filter::Padding(img_mat, 30);
        PGM_P2_255(img_mat).to_file("train/X/" + std::to_string(i) + ".pgm");

        PGM_P2_255 mark = PGM_P2_255(500, 500);
        mark.iplot(Utils::round(triangle[0].getX()), Utils::round(triangle[0].getY()), 255);
        mark.iplot(Utils::round(triangle[1].getX()), Utils::round(triangle[1].getY()), 255);
        mark.iplot(Utils::round(triangle[2].getX()), Utils::round(triangle[2].getY()), 255);


        Mat::mat mark_mat = mark.to_mat();
        // mark_mat = Filter::Padding(mark_mat, 30);
        //mark_mat = Filter::MaxPool(mark_mat, 10);
        mark_mat = Filter::Gaussian(mark_mat, 61, 12);
        mark_mat = Filter::MinMaxScale(mark_mat, 0, 255);
        PGM_P2_255(mark_mat).to_file("train/y/" + std::to_string(i) + ".pgm");

    }

    return 0;
}
*/

/* FILTERS TEST */
/*
int main(int argc, char* argv[])
{
    std::cout << std::fixed;
    PGM_P2_255 img = PGM_P2_255(500, 500);
    img.draw(TriangleGenerator::generate(1, 498));
    // img.draw_noise(0.2);
    img.to_file("image.pgm");

    Mat::mat img_mat = img.to_mat();
    Mat::mat new_img_mat(img_mat);

    //new_img_mat = MeadianFilter(new_img_mat);
    //new_img_mat = MeanFilter(new_img_mat);
    //new_img_mat = GaussianFilter(new_img_mat);
    //new_img_mat = SobelFilter(new_img_mat);
    //new_img_mat = PrewittFilter(new_img_mat);
    //new_img_mat = ThresholdFilter(new_img_mat, 254);

    new_img_mat = Filter::Meadian(new_img_mat);
    new_img_mat = Filter::Meadian(new_img_mat);
    new_img_mat = Filter::Gaussian(new_img_mat);
    new_img_mat = Filter::Gaussian(new_img_mat);
    new_img_mat = Filter::Gaussian(new_img_mat);
    new_img_mat = Filter::Sobel(new_img_mat);
    PGM_P2_255(new_img_mat).to_file("image1.pgm");


    return 0;
}
*/


/* SOLVER FOR PYTORCH */
/*
int main(int argc, char* argv[])
{
    // std::cout << std::fixed;

    double p = std::stof(argv[1]);
    int count = std::stoi(argv[2]);
    for (int i = 0; i < count; i++)
    {
        Triangle triangle = TriangleGenerator::generate(1, 498);

        PGM_P2_255 img = PGM_P2_255(500, 500);
        img.draw(triangle);
        img.draw_noise(p);
        Mat::mat img_mat = img.to_mat();
        // img_mat = Filter::Padding(img_mat, 30);
        PGM_P2_255(img_mat).to_file("train/X/" + std::to_string(i) + ".pgm");

        PGM_P2_255 mark = PGM_P2_255(500, 500);
        mark.iplot(Utils::round(triangle[0].getX()), Utils::round(triangle[0].getY()), 255);
        mark.iplot(Utils::round(triangle[1].getX()), Utils::round(triangle[1].getY()), 255);
        mark.iplot(Utils::round(triangle[2].getX()), Utils::round(triangle[2].getY()), 255);


        Mat::mat mark_mat = mark.to_mat();
        // mark_mat = Filter::Padding(mark_mat, 30);
        //mark_mat = Filter::MaxPool(mark_mat, 10);
        mark_mat = Filter::Gaussian(mark_mat, 61, 12);
        mark_mat = Filter::MinMaxScale(mark_mat, 0, 255);
        PGM_P2_255(mark_mat).to_file("train/y/" + std::to_string(i) + ".pgm");

    }

}
*/

/* SOLVER */

int main(int argc, char* argv[])
{
    // std::cout << std::fixed;

    UNet_predict_only model;
    std::string mode = argv[1];
    if(mode == "-generate")
    {
        double p = std::stof(argv[2]);
        PGM_P2_255 img = PGM_P2_255(500, 500);
        img.draw(TriangleGenerator::generate(1, 498));
        img.draw_noise(p);
        img.to_file("image.pgm");
    }
    else if(mode == "-restore")
    {
        std::string image_path = argv[2];
        std::ifstream is(image_path);
        std::string buffer;
        std::vector<std::string> data;
        while (std::getline(is, buffer))
            if(buffer.size() > 0)
                data.push_back(buffer);

        Mat::mat img(500, Vec::vec(500));
        std::istringstream s(data[3]);
        double d;
        for(int i = 0; i < 500; i++)
            for(int j = 0; j < 500; j++)
                if(s >> d)
                    img[i][j] = Utils::rescaling(d, 0, 255, 0, 1);



        Mat2d::mat2d X = Mat2d::init(Mat2d::size_t(1,1,500,500));
        X[0][0] = img;

        int black_counter = 0;
        for(int j = 0; j < img.size(); j++)
            for(int k = 0; k < img[0].size(); k++)
                if (img[j][k] == 0) 
                    black_counter++;

        if(black_counter < 250000 && black_counter > 230000)
        {
            model.load("unet_weights_0_0");
            // std::cout << "[0.0]\n";
        }
        else if(black_counter < 230000 && black_counter > 221000)
        {
            model.load("unet_weights_0_5");
            // std::cout << "[0.1]\n";
        }
        else if(black_counter < 221000 && black_counter > 191000)
        {
            model.load("unet_weights_0_5");
            // std::cout << "[0.2]\n";
        }
        else if(black_counter < 191000 && black_counter > 155000)
        {
            model.load("unet_weights_0_5");
            // std::cout << "[0.3]\n";
        }
        else if(black_counter < 155000 && black_counter > 132000)
        {
            model.load("unet_weights_0_5");
            // std::cout << "[0.4]\n";
        }
        else if(black_counter < 132000 && black_counter > 110000)
        {
            model.load("unet_weights_0_5");
            // std::cout << "[0.5]\n";
        }
        else
        {
            model.load("unet_weights_0_6");
            // std::cout << "[0.6]\n";
        }

        Mat::mat pred_mat = model.predict(X)[0][0];
        pred_mat = Filter::MinMaxScale(pred_mat, 0, 255);


        double threshold = 170;
        double threshold_step = 1;
        int end_shift = 3;
        int step_count = 200;
        std::vector<Point> points_pred;
        std::vector<std::vector<Point>> points_pred_hist;
        for(int i = 0; i < step_count; i++)
        {
            Mat::mat _pred_mat = Filter::Threshold(pred_mat, threshold);
            
            std::vector<Point> points;
            for(int i = 0; i < _pred_mat.size(); i++)
                for(int j = 0; j < _pred_mat[0].size(); j++)
                    if(_pred_mat[i][j] != 0)
                        points.push_back(Point(j, i));

            points_pred = meanshift(points);
            if(points_pred.size() < 3)
            {
                if(points_pred_hist.size() != 0)
                {
                    // int pred_idx;
                    // if(points_pred_hist.size() < end_shift)
                    //     pred_idx = 0;
                    // else
                    //     pred_idx = points_pred_hist.size() - end_shift;

                    int pred_idx = ((points_pred_hist.size() - 1) / 3) * 2;

                    // int pred_idx = points_pred_hist.size() - 1;

                    points_pred = points_pred_hist[pred_idx];
                    break;
                }
                threshold -= threshold_step;
            }
            else if(points_pred.size() == 3)
            {
                points_pred_hist.push_back(points_pred);
                pred_mat = _pred_mat;
                threshold += threshold_step;
            }
            else
            {
                threshold += threshold_step;
            }
            points_pred = std::vector<Point>{Point(0.0, 0.0), Point(0.0, 0.0), Point(0.0, 0.0)};
        }

        std::ofstream file("output.txt");
        file << points_pred[0].X << " " << points_pred[0].Y << std::endl
             << points_pred[1].X << " " << points_pred[1].Y << std::endl
             << points_pred[2].X << " " << points_pred[2].Y;
        file.close();

    }


    return 0;
}


/* SOLVER OLD */
/*
int main(int argc, char* argv[])
{
    std::cout << std::fixed;

    std::shared_ptr<Model> model(new OneLayerNet(500*500, 6));
    std::string mode = argv[1];
    if(mode == "-generate")
    {
        double p = std::stof(argv[2]);
        PGM_P2_255 img = PGM_P2_255(500, 500);
        img.draw(TriangleGenerator::generate(0, 499, 100, 30));
        img.draw_noise(p);
        img.to_file("image.pgm");
    }
    else if(mode == "-restore")
    {
        std::string image_path = argv[2];
        std::vector<std::vector<double>> X;
        std::ifstream is(image_path);
        std::string buffer;
        std::vector<std::string> data;
        while (std::getline(is, buffer))
            if(buffer.size() > 0)
                data.push_back(buffer);

        std::vector<double> img;
        std::istringstream s(data[3]);
        double d;
        while (s >> d)
            img.push_back(d);

        X.push_back(img);

        model->load();
        StandartScaling stand_X = StandartScaling(80.653100, 57.817638);
        std::vector<std::vector<double>> y_pred = model->predict(stand_X.transform(X));
        std::ofstream file("output.txt");
        file << y_pred[0][0] << " " << y_pred[0][1] << std::endl
             << y_pred[0][2] << " " << y_pred[0][3] << std::endl
             << y_pred[0][4] << " " << y_pred[0][5];
        file.close();

    }
    else if(mode == "-train")
    {
        std::cout << "Data load ..." << std::endl;
        std::shared_ptr<Dataset> data = load_triangle_dataset();

        int num_epochs = 200;
        int batch_size = 8;
        double learning_rate = 0.00001;
        double learning_rate_decay = 0.88;
        int decey_step = 14;
        double learning_rate_decay_1 = 1;
        int decey_step_1 = 1;

        std::shared_ptr<Dataset> dataset(data);
        std::shared_ptr<Optimizer> optim(new MomentumSGD(0.7));
        Trainer trainer(model, dataset, optim, num_epochs, batch_size, learning_rate, learning_rate_decay, decey_step, learning_rate_decay_1, decey_step_1);
        
        std::cout << "Model fit ..." << std::endl;
        trainer.fit();
        model->save();
    }


    return 0;
}
*/