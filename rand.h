#ifndef RAND_H
#define RAND_H

#include <iostream>
#include <random>
#include <chrono>
#include <utility>
#include "utils.h"


class Random
{

private:

    static std::mt19937 random_generator;

public:

	typedef int result_type;
	
	static int min() 
	{ 
		return Utils::int_min;
	}

	static int max() 
	{
		return Utils::int_max;
	}
	
	int operator()()
	{
    	return random_int(min(), max());
    }

    static double random_double(int start, int end)
    {
        std::uniform_real_distribution<> distribution(start, end);
        return distribution(random_generator);
    }

    static int random_int(int start, int end)
    {
        std::uniform_int_distribution<> distribution(start, end);
        return distribution(random_generator);
    }

};

std::mt19937 Random::random_generator = std::mt19937(
    std::chrono::duration_cast<std::chrono::nanoseconds>(
        std::chrono::high_resolution_clock::now().time_since_epoch()).count());


#endif