#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <vector>


namespace Utils
{

    const int int_max = std::numeric_limits<int>::max();
    const int int_min = std::numeric_limits<int>::min();

    const double double_max = std::numeric_limits<double>::max();
    const double double_min = -std::numeric_limits<double>::max();    

    const double PI = std::atan(1.0) * 4;


    double rescaling(double value, double from1, double from2, double to1, double to2)
    {
        return (value - from1) / (from2 - from1) * (to2 - to1) + to1;
    }


    int ipart(double num)
    {
        return floor(num);
    }


    int round(double num)
    {
        return ipart(num + 0.5);
    }


    double fpart(double num)
    {
        return num - ipart(num);
    }


    template<typename T>
    std::vector<std::vector<T>> split_vector(const std::vector<T>& vec, size_t n)
    {
        std::vector<std::vector<T>> outVec;

        size_t length = vec.size() / n;
        size_t remain = vec.size() % n;

        size_t begin = 0;
        size_t end = 0;

        for (size_t i = 0; i < std::min(n, vec.size()); ++i)
        {
            end += (remain > 0) ? (length + !!(remain--)) : length;

            outVec.push_back(std::vector<T>(vec.begin() + begin, vec.begin() + end));

            begin = end;
        }

        return outVec;
    }

};


#endif