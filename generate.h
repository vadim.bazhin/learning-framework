#include <iostream>
#include <vector>
#include <fstream>
#include <random>
#include <chrono>
#include <cmath>
#include <utility>
#include <stdexcept>
#include "rand.h"
#include "utils.h"


class Point
{

public:

    double X, Y;

    Point(double x, double y)
    {
        X = x;
        Y = y;
    }

    const double &getX() const
    {
        return X;
    }

    const double &getY() const
    {
        return Y;
    }

    double distance(const Point &point) const
    {
        const double x_diff = X - point.getX();
        const double y_diff = Y - point.getY();
        return std::sqrt(x_diff * x_diff + y_diff * y_diff);
    }

};


class Triangle
{

private:

    std::vector<Point> points;

public:

    Triangle(Point p1, Point p2, Point p3)
    {
        points = std::vector<Point>{p1, p2, p3};
    }

    const Point &operator[] (int item) const
    {
        return points[item];
    }

    std::vector<double> getDistances() const
    {
        return std::vector<double>
        {
            points[0].distance(points[1]), 
            points[1].distance(points[2]), 
            points[2].distance(points[0])
        };
    }

    std::vector<double> getAngles() const
    {
        std::vector<double> dist = getDistances();
        double angle1 = std::acos((double)(std::pow(dist[0], 2) + std::pow(dist[2], 2) - std::pow(dist[1], 2)) / (2.0 * dist[0] * dist[2])) * (180.0 / Utils::PI);
        double angle2 = std::acos((double)(std::pow(dist[0], 2) + std::pow(dist[1], 2) - std::pow(dist[2], 2)) / (2.0 * dist[0] * dist[1])) * (180.0 / Utils::PI);
        double angle3 = 180 - (angle1 + angle2);
        return std::vector<double>{angle1, angle2, angle3};
    }

    void to_file(std::string file_name)
    {
        std::ofstream file(file_name);
        for(auto i: to_vector())
                file << i << " ";
        file.close();
    }

    std::vector<double> to_vector()
    {
        //return std::vector<double>{points[0].getX(), points[0].getY()};


        std::vector<double> result;
        for(int i = 0; i < points.size(); i++)
        {
            result.push_back(points[i].getX());
            result.push_back(points[i].getY());
        }
        return result;


        // double x_max = 0;
        // int idx_max = 0;
        // for(int i = 0; i < points.size(); i++)
        // {
        //     if(points[i].getX() > x_max)
        //     {
        //         x_max = points[i].getX();
        //         idx_max = i;
        //     }
        // }
        
        // double x_min = x_max;
        // int idx_min = 0;
        // for(int i = 0; i < points.size(); i++)
        // {
        //     if(points[i].getX() < x_min)
        //     {
        //         x_min = points[i].getX();
        //         idx_min = i;
        //     }
        // }

        // int idx_other = 0;
        // for(int i = 0; i < points.size(); i++)
        // {
        //     if(i != idx_min && i != idx_max)
        //         idx_other = i;
        // }

        // return std::vector<double>{points[idx_max].getX(), points[idx_max].getY(),
        //                            points[idx_min].getX(), points[idx_min].getY(),
        //                            points[idx_other].getX(), points[idx_other].getY()};


    }

};


class TriangleGenerator
{

public:

    static Triangle generate(int start, int end, double min_distance = 100, double min_angle = 30)
    {
        while(true)
        {
            std::vector<Point> points;
            for(int i = 0; i < 3; i++)
            {
                double x = Random::random_double(start, end);
                double y = Random::random_double(start, end);
                points.push_back(Point(x, y));
            }
            Triangle triangle (points[0], points[1], points[2]);
            if(distance_check(triangle, min_distance) && angle_check(triangle, min_angle))
                return triangle;
        }
    }

private:

    static bool distance_check(const Triangle &triangle, double value)
    {
        std::vector<double> dist = triangle.getDistances();
        for (const double &i: dist)
            if(i < value)
                return false;
        return true;
    }

    static bool angle_check(const Triangle &triangle, double value)
    {
        std::vector<double> angl = triangle.getAngles();
        for (const double &i: angl)
            if(i < value)
                return false;
        return true;
    }

};


class PGM_P2_255
{

private:

    int width, height;
    std::vector<std::vector<int>> image;

public:

    PGM_P2_255(int width, int height) 
    {
        this->width = width;
        this->height = height;
        image.resize(height, std::vector<int>(width, 0));
    }

    PGM_P2_255(std::vector<std::vector<double>> image) 
    {
        for(int i = 0; i < image.size(); i++)
        {
            std::vector<int> buf;
            for(int j = 0; j < image[i].size(); j++)
                buf.push_back(image[i][j]);
            this->image.push_back(buf);
        }
        this->width = image[0].size();
        this->height = image.size();
    }

    std::vector<int> &operator[] (int item)
    {
        return image[item];
    }

    void to_file(std::string file_name)
    {
        std::ofstream file(file_name);
        file << "P2" << std::endl
             << width << " " << height << std::endl
             << "255" << std::endl;
        for(auto i: image)
            for(auto j: i)
                file << j << " ";
        file.close();
    }

    std::vector<double> to_vector()
    {
        std::vector<double> img;
        for(int i = 0; i < image.size(); i++)
            for(int j = 0; j < image[i].size(); j++)
                img.push_back(image[i][j]);
        return img;
    }

    std::vector<std::vector<double>> to_mat()
    {
        std::vector<std::vector<double>> result;
        for(int i = 0; i < image.size(); i++)
        {
            std::vector<double> buf;
            for(int j = 0; j < image[i].size(); j++)
                buf.push_back(image[i][j]);
            result.push_back(buf);
        }
        return result;
    }

    void draw(Triangle triangle)
    {
        draw(triangle[0], triangle[1]);
        draw(triangle[1], triangle[2]);
        draw(triangle[2], triangle[0]);
    }

    void draw(Point point1, Point point2)
    {
        draw(point1.getX(), point1.getY(), point2.getX(), point2.getY());
    }

    void draw(double x1, double y1, double x2, double y2)
    {
        bool steep = abs(y2 - y1) > abs(x2 - x1);

        if(steep)
        {
            std::swap(x1, y1);
            std::swap(x2, y2);
        }

        if(x1 > x2)
        {
            std::swap(x1, x2);
            std::swap(y1, y2);
        }

        if(Utils::ipart(x2 - x1) == 0)
        {
            fplot(Utils::round(x1), Utils::round(y1), 1);
            return;
        }

        double dx = x2 - x1;
        double dy = y2 - y1;
        double gradient = dy / dx;

        int xend = Utils::round(x1);
        double yend = y1 + gradient * (xend - x1);
        double xgap = 1 - Utils::fpart(x1 + 0.5);
        int xpxl1 = xend;
        int ypxl1 = Utils::ipart(yend);

        if(steep)
        {
            fplot(ypxl1, xpxl1, (1 - Utils::fpart(yend)) * xgap);
            fplot(ypxl1 + 1, xpxl1, Utils::fpart(yend) * xgap);
        }
        else
        {
            fplot(xpxl1, ypxl1, (1 - Utils::fpart(yend)) * xgap);
            fplot(xpxl1, ypxl1 + 1, Utils::fpart(yend) * xgap);
        }
        double intery = yend + gradient;

        xend = Utils::round(x2);
        yend = y2 + gradient * (xend - x2);
        xgap = Utils::fpart(x2 + 0.5);
        int xpxl2 = xend;
        int ypxl2 = Utils::ipart(yend);

        if(steep)
        {
            fplot(ypxl2, xpxl2, (1 - Utils::fpart(yend)) * xgap);
            fplot(ypxl2 + 1, xpxl2, Utils::fpart(yend)* xgap);
        }
        else
        {
            fplot(xpxl2, ypxl2, (1 - Utils::fpart(yend)) * xgap);
            fplot(xpxl2, ypxl2 + 1, Utils::fpart(yend)* xgap);   
        }

        for(int x = (xpxl1 + 1); x <= (xpxl2 - 1); x++)
        {
            if(steep)
            {
                fplot(Utils::ipart(intery), x, 1 - Utils::fpart(intery));
                fplot(Utils::ipart(intery) + 1, x, Utils::fpart(intery));
            }
            else
            {
                fplot(x, Utils::ipart(intery), 1 - Utils::fpart(intery));
                fplot(x, Utils::ipart(intery) + 1, Utils::fpart(intery));
            }
            intery += gradient;
        }

    }

    void draw_noise(double probability)
    {
        if(probability > 1 || probability < 0)
            throw std::invalid_argument("1 >= probability >= 0");

        for(int i = 0; i < image.size(); i++)
            for(int j = 0; j < image[i].size(); j++)
                if(Random::random_int(1, 100) <= probability * 100)
                    image[i][j] = Random::random_int(0, 255);
    }

    void fplot(int x, int y, double c)
    {
        image[y][x] = Utils::rescaling(c, 0, 1, 0, 255);
    }

    void iplot(int x, int y, int c)
    {
        image[y][x] = c;
    }

};