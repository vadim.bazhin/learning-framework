#ifndef TRAINER_H
#define TRAINER_H

#include <iostream>
#include <algorithm>
#include "models.h"
#include "dataset.h"
#include "optim.h"
#include "rand.h"
#include "utils.h"
#include "layers.h"


class Trainer
{

private:

	std::shared_ptr<Model> model;
	std::shared_ptr<Dataset> dataset;
	std::shared_ptr<Optimizer> optim;
	int num_epochs;
	int batch_size;
	double learning_rate;
	double learning_rate_decay;
	int decay_step;
	double learning_rate_decay1;
	int decay_step1;

public:

	Trainer(std::shared_ptr<Model> model,
			std::shared_ptr<Dataset> dataset,
			std::shared_ptr<Optimizer> optim,
			int num_epochs,
			int batch_size,
			double learning_rate,
			double learning_rate_decay,
			int decay_step,
			double learning_rate_decay1,
			int decay_step1):
		model(model),
		dataset(dataset),
		optim(optim),
		learning_rate_decay(1),
		decay_step(1),
		learning_rate_decay1(1),
		decay_step1(1)
	{
		this->num_epochs = num_epochs;
		this->batch_size = batch_size;
		this->learning_rate = learning_rate;
		this->learning_rate_decay = learning_rate_decay;
		this->decay_step = decay_step;
		this->learning_rate_decay1 = learning_rate_decay1;
		this->decay_step1 = decay_step1;
	}

	double compute_distance(const std::vector<std::vector<double>> &y_pred, const std::vector<std::vector<double>> &y)
	{
		double result = 0;
		for(int i = 0; i < y_pred.size(); i++)
		{
			double distance_max = 0;
			for(int j = 0; j < y_pred[i].size(); j += 2)
			{
				double x1 = y_pred[i][j];
				double y1 = y_pred[i][j+1];
				double x2 = y[i][j];
				double y2 = y[i][j+1];
				double x_diff = x1 - x2;
				double y_diff = y1 - y2;
				double distance = std::sqrt(x_diff * x_diff + y_diff * y_diff);
				if(distance_max < distance)
					distance_max = distance;
			}
			result += distance_max;
		}
		return result / y_pred.size();
	}

	void fit()
	{
		int num_train = dataset->train_X.size();

		for(int epoch = 1, decay_step_it = 0, decay_step_it1 = 0; epoch <= num_epochs; epoch++, decay_step_it++, decay_step_it1++)
		{
			double loss = 0;

			std::vector<int> shuffled_indices(num_train);
			std::iota(std::begin(shuffled_indices), std::end(shuffled_indices), 0);
			std::shuffle(std::begin(shuffled_indices), std::end(shuffled_indices), Random());

			int num_batch_split = num_train / batch_size;
			num_batch_split += (num_train % batch_size != 0) ? 1 : 0;
			std::vector<std::vector<int>> batches_indices = Utils::split_vector<int>(shuffled_indices, num_batch_split);

			for(std::vector<int> batch_indices: batches_indices)
			{
				loss += model->compute_loss_and_gradients(dataset->get_batch_x(batch_indices), dataset->get_batch_y(batch_indices));
				
				double it = 0;
				auto params = model->params();
				for(auto &param: params)
				{
					if(it == 1)
						param->value = optim->update(param->value, param->grad, learning_rate * 10000.0);
					else
						param->value = optim->update(param->value, param->grad, learning_rate);
					it += 0.5;
				}
			}

			if(learning_rate_decay != 1 && decay_step_it == decay_step)
			{
				learning_rate *= learning_rate_decay;
				decay_step_it = 0;
			}

			if(learning_rate_decay1 != 1 && decay_step_it1 == decay_step1)
			{
				learning_rate *= learning_rate_decay1;
				decay_step_it1 = 0;
			}

			
			loss /= num_batch_split;
			double dist = compute_distance(model->predict(dataset->train_X), dataset->train_y);
			double val_dist = compute_distance(model->predict(dataset->val_X), dataset->val_y);

			std::cout << "epoch: " << epoch
					  << ", loss: " << loss
					  << ", dist: " << dist
					  << ", val_dist: " << val_dist
					  << std::endl;
		}
	}


};


// class UNetTrainer
// {

// private:

// 	UNet model;
// 	std::shared_ptr<Dataset2d> dataset;
// 	SGD2d optim;
// 	int num_epochs;
// 	int batch_size;
// 	double learning_rate;

// public:

// 	UNetTrainer(std::shared_ptr<Dataset2d> dataset,
// 				int num_epochs,
// 				int batch_size,
// 				double learning_rate): dataset(dataset)
// 	{
// 		this->num_epochs = num_epochs;
// 		this->batch_size = batch_size;
// 		this->learning_rate = learning_rate;
// 	}


// 	void fit()
// 	{
// 		int num_train = dataset->train_X.size();

// 		for(int epoch = 1; epoch <= num_epochs; epoch++)
// 		{
// 			double loss = 0;

// 			std::vector<int> shuffled_indices(num_train);
// 			std::iota(std::begin(shuffled_indices), std::end(shuffled_indices), 0);
// 			std::shuffle(std::begin(shuffled_indices), std::end(shuffled_indices), Random());

// 			int num_batch_split = num_train / batch_size;
// 			num_batch_split += (num_train % batch_size != 0) ? 1 : 0;
// 			std::vector<std::vector<int>> batches_indices = Utils::split_vector<int>(shuffled_indices, num_batch_split);

// 			for(std::vector<int> batch_indices: batches_indices)
// 			{
// 				loss += model.compute_loss_and_gradients(dataset->get_batch_x(batch_indices), dataset->get_batch_y(batch_indices));
				

// 				auto params = model.params();
// 				for(auto &param: params)
// 						param->value = optim.update(param->value, param->grad, learning_rate);
// 			}

			
// 			loss /= num_batch_split;


// 			std::cout << "epoch: " << epoch
// 					  << ", loss: " << loss
// 					  << std::endl;
// 		}
// 	}


// };


#endif