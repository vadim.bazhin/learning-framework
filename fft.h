#ifndef FFT_H
#define FFT_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>


namespace fft
{


#define M_PI 3.14159265358979323846

typedef struct { double r; double i; } w_type;

int n2ln(int n)
{
    int ln = 0;
    while (n >>= 1) ln++;
    return ln;
}

void w_mac(w_type* cc, w_type a, w_type w, w_type b)
{
    register double reg;
    reg = a.r + w.r * b.r;
    cc->r = reg + (-w.i * b.i);
    reg = a.i + w.r * b.i;
    cc->i = reg + w.i * b.r;
}

static void fft0(w_type InOut[], int n) 
{ 
    int i;
    w_type w, *A, *B;

    if (n == 1) return; 
    A = (w_type*)malloc(sizeof(w_type) * n / 2);
    B = (w_type*)malloc(sizeof(w_type) * n / 2);
    for (i = 0; i < n / 2; i++) 
    {
        A[i] = InOut[i * 2];
        B[i] = InOut[i * 2 + 1];
    }
    fft0(A, n / 2);
    fft0(B, n / 2);
    for (i = 0; i < n; i++) 
    {
        w.r = cos(2 * M_PI * i / n);
        w.i = sin(2 * M_PI * i / n);
        w_mac(&InOut[i], A[i % (n / 2)], w, B[i % (n / 2)]);
    }
    free(A);
    free(B);
}

static void swap(w_type* arg1, w_type* arg2)
{
    w_type buffer = *arg1;
    *arg1 = *arg2;
    *arg2 = buffer;
}

static void transpose(w_type* InOut[], int n)
{
    int i, j;
    for(i = 0; i < n; i++)
        for(j = i+1; j < n; j++)
            swap(&(InOut[i][j]), &(InOut[j][i]));
}

static void print(w_type* InOut[], int n)
{
    int i, j;
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
            printf("%.4f (%.4fi)  ", InOut[i][j].r, InOut[i][j].i);
        printf("\n");
    }
}

static void fft2(w_type* InOut[], int n)
{
    int i, j;
    for(i = 0; i < n; i++)
        fft0(InOut[i], n);

    transpose(InOut, n);

    for(i = 0; i < n; i++)
        fft0(InOut[i], n);

    transpose(InOut, n);
}

static void conj(w_type* InOut[], int n)
{
    int i, j;
    for(i = 0; i < n; i++)
        for(j = 0; j < n; j++)
            InOut[i][j].i = -InOut[i][j].i;
}

static void ifft2(w_type* InOut[], int n)
{
    conj(InOut, n);
    fft2(InOut, n);
    conj(InOut, n);
}

static void prod(w_type* Out[], w_type* In[], int n)
{
    int i, j;
    for(i = 0; i < n; i++)
        for(j = 0; j < n; j++)
        {
            register double r_out = Out[i][j].r * In[i][j].r - Out[i][j].i * In[i][j].i;
            register double i_out = Out[i][j].r * In[i][j].i + Out[i][j].i * In[i][j].r;
            Out[i][j].r = r_out;
            Out[i][j].i = i_out;
        }
}


};


#endif