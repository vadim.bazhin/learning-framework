#ifndef MODELS_H
#define MODELS_H

#include <iostream>
#include "layers.h"
#include "mat.h"


class Model
{

public:

    virtual Mat::mat forward(const Mat::mat &X) = 0;
    virtual Mat::mat backward(const Mat::mat &d_out) = 0;
    virtual std::vector<std::shared_ptr<Weights>> params() = 0;
    virtual double compute_loss_and_gradients(const Mat::mat &X, const Mat::mat &y) = 0;
    virtual Mat::mat predict(const Mat::mat &X) = 0;

    void save()
    {
        std::vector<std::shared_ptr<Weights>> weights = params();
        for(int i = 0; i < weights.size(); i++)
        {
            Vec::vec buf;
            for(int j = 0; j < weights[i]->value.size(); j++)
                for(int k = 0; k < weights[i]->value[j].size(); k++)
                    buf.push_back(weights[i]->value[j][k]);

            std::ofstream file("model_weights_" + std::to_string(i), std::ios::binary);
            file.write(reinterpret_cast<char*>(buf.data()), buf.size()*sizeof(double));
            file.close();
        }
    }

    void load()
    {
        std::vector<std::shared_ptr<Weights>> weights = params();
        for(int i = 0; i < weights.size(); i++)
        {
            Vec::vec buf(weights[i]->value.size() * weights[i]->value[0].size());
            std::ifstream file("model_weights_" + std::to_string(i), std::ios::binary);
            file.read(reinterpret_cast<char*>(&buf.front()), buf.size()*sizeof(uint64_t));
            file.close();

            for(int j = 0, n = 0; j < weights[i]->value.size(); j++)
                for(int k = 0; k < weights[i]->value[j].size(); k++, n++)
                    weights[i]->value[j][k] = buf[n];
        }
    }

};


class TwoLayerNet : public Model
{

private:

    double reg;
    std::vector<std::unique_ptr<Layer>> layers;

public:

    TwoLayerNet(int n_input, int n_output, int hidden_layer_size, double reg)
    {
        this->reg = reg;
        layers.push_back(std::unique_ptr<Layer>(new FullyConnectedLayer(n_input, hidden_layer_size)));
        layers.push_back(std::unique_ptr<Layer>(new LeakyReLULayer()));
        layers.push_back(std::unique_ptr<Layer>(new FullyConnectedLayer(hidden_layer_size, n_output)));
    }

    Mat::mat forward(const Mat::mat &X)
    {
        Mat::mat x(X);
        for(int i = 0; i < layers.size(); i++)
            x = layers[i]->forward(x);
        return x;
    }

    Mat::mat backward(const Mat::mat &d_out)
    {
        Mat::mat grad(d_out);
        for(int i = layers.size() - 1; i >= 0; i--)
            grad = layers[i]->backward(grad);
        return grad;
    }

    double compute_loss_and_gradients(const Mat::mat &X, const Mat::mat &y)
    {
        std::vector<std::shared_ptr<Weights>> v_params = params();
        for(int i = 0; i < v_params.size(); i++)
            v_params[i]->reset_grad();

        Mat::mat pred = forward(X);
        Loss loss = distance_loss(pred, y);
        Mat::mat grad = backward(loss.grad);

        for(int i = 0; i < v_params.size(); i++)
        {
            Loss l2_loss = l2_regularization(v_params[i]->value, reg);
            loss.value += l2_loss.value;
            v_params[i]->grad = Mat::sum(v_params[i]->grad, l2_loss.grad);
        }

        return loss.value;
    }

    Mat::mat predict(const Mat::mat &X)
    {
        return forward(X);
    }

    std::vector<std::shared_ptr<Weights>> params()
    {
        std::vector<std::shared_ptr<Weights>> result;
        for(int i = 0; i < layers.size(); i++)
        {
            std::vector<std::shared_ptr<Weights>> item_params(layers[i]->params());
            int item_params_size = item_params.size();
            for(int j = 0; j < item_params_size; j++)
                result.push_back(item_params[j]);
        }
        return result;
    }

};


class OneLayerNet : public Model
{

private:

    double reg;
    std::unique_ptr<Layer> fc;

public:

    OneLayerNet(int n_input, int n_output, double reg = 0):
        fc(std::unique_ptr<Layer>(new FullyConnectedLayer(n_input, n_output))),
        reg(reg)
        {}

    Mat::mat forward(const Mat::mat &X)
    {
        return fc->forward(X);
    }

    Mat::mat backward(const Mat::mat &d_out)
    {
        return fc->backward(d_out);
    }

    double compute_loss_and_gradients(const Mat::mat &X, const Mat::mat &y)
    {
        std::vector<std::shared_ptr<Weights>> v_params = params();
        for(int i = 0; i < v_params.size(); i++)
            v_params[i]->reset_grad();

        Mat::mat pred = forward(X);
        Loss loss = mean_squared_error(pred, y);
        //Loss loss = distance_loss(pred, y);
        Mat::mat grad = backward(loss.grad);

        for(int i = 0; i < v_params.size(); i++)
        {
            Loss l2_loss = l2_regularization(v_params[i]->value, reg);
            loss.value += l2_loss.value;
            v_params[i]->grad = Mat::sum(v_params[i]->grad, l2_loss.grad);
        }

        return loss.value;
    }

    Mat::mat predict(const Mat::mat &X)
    {
        return forward(X);
    }

    std::vector<std::shared_ptr<Weights>> params()
    {
        return fc->params();
    }

};


class UNet
{

private:

    double reg;

    std::unique_ptr<Conv2d> inc_conv1 = std::unique_ptr<Conv2d>(new Conv2d(1, 64, 3, 1));
    std::unique_ptr<BatchNorm2d> inc_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(64));
    std::unique_ptr<ReLU2d> inc_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());

    std::unique_ptr<Conv2d> inc_conv2 = std::unique_ptr<Conv2d>(new Conv2d(64, 64, 3, 1));
    std::unique_ptr<BatchNorm2d> inc_norm2 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(64));
    std::unique_ptr<ReLU2d> inc_relu2 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<MaxPool2d> down1_pool = std::unique_ptr<MaxPool2d>(new MaxPool2d(2));

    std::unique_ptr<Conv2d> down1_conv1 = std::unique_ptr<Conv2d>(new Conv2d(64, 128, 3, 1));
    std::unique_ptr<BatchNorm2d> down1_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(128));
    std::unique_ptr<ReLU2d> down1_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());

    std::unique_ptr<Conv2d> down1_conv2 = std::unique_ptr<Conv2d>(new Conv2d(128, 128, 3, 1));
    std::unique_ptr<BatchNorm2d> down1_norm2 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(128));
    std::unique_ptr<ReLU2d> down1_relu2 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<MaxPool2d> down2_pool = std::unique_ptr<MaxPool2d>(new MaxPool2d(2));

    std::unique_ptr<Conv2d> down2_conv1 = std::unique_ptr<Conv2d>(new Conv2d(128, 256, 3, 1));
    std::unique_ptr<BatchNorm2d> down2_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(256));
    std::unique_ptr<ReLU2d> down2_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());

    std::unique_ptr<Conv2d> down2_conv2 = std::unique_ptr<Conv2d>(new Conv2d(256, 256, 3, 1));
    std::unique_ptr<BatchNorm2d> down2_norm2 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(256));
    std::unique_ptr<ReLU2d> down2_relu2 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<MaxPool2d> down3_pool = std::unique_ptr<MaxPool2d>(new MaxPool2d(2));

    std::unique_ptr<Conv2d> down3_conv1 = std::unique_ptr<Conv2d>(new Conv2d(256, 512, 3, 1));
    std::unique_ptr<BatchNorm2d> down3_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(512));
    std::unique_ptr<ReLU2d> down3_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());

    std::unique_ptr<Conv2d> down3_conv2 = std::unique_ptr<Conv2d>(new Conv2d(512, 512, 3, 1));
    std::unique_ptr<BatchNorm2d> down3_norm2 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(512));
    std::unique_ptr<ReLU2d> down3_relu2 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<MaxPool2d> down4_pool = std::unique_ptr<MaxPool2d>(new MaxPool2d(2));

    std::unique_ptr<Conv2d> down4_conv1 = std::unique_ptr<Conv2d>(new Conv2d(512, 512, 3, 1));
    std::unique_ptr<BatchNorm2d> down4_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(512));
    std::unique_ptr<ReLU2d> down4_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());

    std::unique_ptr<Conv2d> down4_conv2 = std::unique_ptr<Conv2d>(new Conv2d(512, 512, 3, 1));
    std::unique_ptr<BatchNorm2d> down4_norm2 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(512));
    std::unique_ptr<ReLU2d> down4_relu2 = std::unique_ptr<ReLU2d>(new ReLU2d());



    std::unique_ptr<Upsample2d> up1_upsample = std::unique_ptr<Upsample2d>(new Upsample2d(2));

    std::unique_ptr<Conv2d> up1_conv1 = std::unique_ptr<Conv2d>(new Conv2d(1024, 256, 3, 1));
    std::unique_ptr<BatchNorm2d> up1_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(256));
    std::unique_ptr<ReLU2d> up1_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());

    std::unique_ptr<Conv2d> up1_conv2 = std::unique_ptr<Conv2d>(new Conv2d(256, 256, 3, 1));
    std::unique_ptr<BatchNorm2d> up1_norm2 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(256));
    std::unique_ptr<ReLU2d> up1_relu2 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<Upsample2d> up2_upsample = std::unique_ptr<Upsample2d>(new Upsample2d(2));

    std::unique_ptr<Conv2d> up2_conv1 = std::unique_ptr<Conv2d>(new Conv2d(512, 128, 3, 1));
    std::unique_ptr<BatchNorm2d> up2_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(128));
    std::unique_ptr<ReLU2d> up2_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());

    std::unique_ptr<Conv2d> up2_conv2 = std::unique_ptr<Conv2d>(new Conv2d(128, 128, 3, 1));
    std::unique_ptr<BatchNorm2d> up2_norm2 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(128));
    std::unique_ptr<ReLU2d> up2_relu2 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<Upsample2d> up3_upsample = std::unique_ptr<Upsample2d>(new Upsample2d(2));

    std::unique_ptr<Conv2d> up3_conv1 = std::unique_ptr<Conv2d>(new Conv2d(256, 64, 3, 1));
    std::unique_ptr<BatchNorm2d> up3_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(64));
    std::unique_ptr<ReLU2d> up3_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());

    std::unique_ptr<Conv2d> up3_conv2 = std::unique_ptr<Conv2d>(new Conv2d(64, 64, 3, 1));
    std::unique_ptr<BatchNorm2d> up3_norm2 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(64));
    std::unique_ptr<ReLU2d> up3_relu2 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<Upsample2d> up4_upsample = std::unique_ptr<Upsample2d>(new Upsample2d(2));

    std::unique_ptr<Conv2d> up4_conv1 = std::unique_ptr<Conv2d>(new Conv2d(128, 64, 3, 1));
    std::unique_ptr<BatchNorm2d> up4_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(64));
    std::unique_ptr<ReLU2d> up4_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());

    std::unique_ptr<Conv2d> up4_conv2 = std::unique_ptr<Conv2d>(new Conv2d(64, 64, 3, 1));
    std::unique_ptr<BatchNorm2d> up4_norm2 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(64));
    std::unique_ptr<ReLU2d> up4_relu2 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<Conv2d> outc = std::unique_ptr<Conv2d>(new Conv2d(64, 1, 1, 0));


public:

    UNet(double reg = 0)
    {
        this->reg = reg;
    }

    double compute_loss_and_gradients(const Mat2d::mat2d &X, const Mat2d::mat2d &y)
    {
        std::vector<std::shared_ptr<Weights2d>> v_params = params();
        for(int i = 0; i < v_params.size(); i++)
            v_params[i]->reset_grad();

        Mat2d::mat2d pred = forward(X);
        Loss2d loss = mean_squared_error(pred, y);
        Mat2d::mat2d grad = backward(loss.grad);

        for(int i = 0; i < v_params.size(); i++)
        {
            Loss2d l2_loss = l2_regularization(v_params[i]->value, reg);
            loss.value += l2_loss.value;
            v_params[i]->grad = Mat2d::sum(v_params[i]->grad, l2_loss.grad);
        }

        return loss.value;
    }

    Mat2d::mat2d forward(const Mat2d::mat2d &X)
    {
        Mat2d::mat2d out = inc_conv1->forward(X);
        out = inc_norm1->forward(out);
        out = inc_relu1->forward(out);

        out = inc_conv2->forward(out);
        out = inc_norm2->forward(out);
        out = inc_relu2->forward(out);
        Mat2d::mat2d inc_out = out;


        out = down1_pool->forward(out);

        out = down1_conv1->forward(out);
        out = down1_norm1->forward(out);
        out = down1_relu1->forward(out);

        out = down1_conv2->forward(out);
        out = down1_norm2->forward(out);
        out = down1_relu2->forward(out);
        Mat2d::mat2d down1_out = out;


        out = down2_pool->forward(out);

        out = down2_conv1->forward(out);
        out = down2_norm1->forward(out);
        out = down2_relu1->forward(out);

        out = down2_conv2->forward(out);
        out = down2_norm2->forward(out);
        out = down2_relu2->forward(out);
        Mat2d::mat2d down2_out = out;


        out = down3_pool->forward(out);

        out = down3_conv1->forward(out);
        out = down3_norm1->forward(out);
        out = down3_relu1->forward(out);

        out = down3_conv2->forward(out);
        out = down3_norm2->forward(out);
        out = down3_relu2->forward(out);
        Mat2d::mat2d down3_out = out;


        out = down4_pool->forward(out);

        out = down4_conv1->forward(out);
        out = down4_norm1->forward(out);
        out = down4_relu1->forward(out);

        out = down4_conv2->forward(out);
        out = down4_norm2->forward(out);
        out = down4_relu2->forward(out);



        out = up1_upsample->forward(out);
        out = cat_channel(out, down3_out);

        out = up1_conv1->forward(out);
        out = up1_norm1->forward(out);
        out = up1_relu1->forward(out);

        out = up1_conv2->forward(out);
        out = up1_norm2->forward(out);
        out = up1_relu2->forward(out);


        out = up2_upsample->forward(out);
        out = cat_channel(out, down2_out);

        out = up2_conv1->forward(out);
        out = up2_norm1->forward(out);
        out = up2_relu1->forward(out);

        out = up2_conv2->forward(out);
        out = up2_norm2->forward(out);
        out = up2_relu2->forward(out);


        out = up3_upsample->forward(out);
        out = cat_channel(out, down1_out);

        out = up3_conv1->forward(out);
        out = up3_norm1->forward(out);
        out = up3_relu1->forward(out);

        out = up3_conv2->forward(out);
        out = up3_norm2->forward(out);
        out = up3_relu2->forward(out);


        out = up4_upsample->forward(out);
        out = cat_channel(out, inc_out);

        out = up4_conv1->forward(out);
        out = up4_norm1->forward(out);
        out = up4_relu1->forward(out);

        out = up4_conv2->forward(out);
        out = up4_norm2->forward(out);
        out = up4_relu2->forward(out);


        out = outc->forward(out);

        return out;
    }

    Mat2d::mat2d backward(const Mat2d::mat2d &d_out)
    {
        Mat2d::mat2d out = outc->backward(d_out);


        out = up4_relu2->backward(out);
        out = up4_norm2->backward(out);
        out = up4_conv2->backward(out);

        out = up4_relu1->backward(out);
        out = up4_norm1->backward(out);
        out = up4_conv1->backward(out);

        std::vector<Mat2d::mat2d> up4_out = uncat_channel(out);
        out = up4_upsample->backward(up4_out[0]);


        out = up3_relu2->backward(out);
        out = up3_norm2->backward(out);
        out = up3_conv2->backward(out);

        out = up3_relu1->backward(out);
        out = up3_norm1->backward(out);
        out = up3_conv1->backward(out);

        std::vector<Mat2d::mat2d> up3_out = uncat_channel(out);
        out = up3_upsample->backward(up3_out[0]);


        out = up2_relu2->backward(out);
        out = up2_norm2->backward(out);
        out = up2_conv2->backward(out);

        out = up2_relu1->backward(out);
        out = up2_norm1->backward(out);
        out = up2_conv1->backward(out);

        std::vector<Mat2d::mat2d> up2_out = uncat_channel(out);
        out = up2_upsample->backward(up2_out[0]);


        out = up1_relu2->backward(out);
        out = up1_norm2->backward(out);
        out = up1_conv2->backward(out);

        out = up1_relu1->backward(out);
        out = up1_norm1->backward(out);
        out = up1_conv1->backward(out);

        std::vector<Mat2d::mat2d> up1_out = uncat_channel(out);
        out = up1_upsample->backward(up1_out[0]);



        out = down4_relu2->backward(out);
        out = down4_norm2->backward(out);
        out = down4_conv2->backward(out);

        out = down4_relu1->backward(out);
        out = down4_norm1->backward(out);
        out = down4_conv1->backward(out);

        out = down4_pool->backward(out);


        out = Mat2d::sum(out, up1_out[1]);
        out = down3_relu2->backward(out);
        out = down3_norm2->backward(out);
        out = down3_conv2->backward(out);

        out = down3_relu1->backward(out);
        out = down3_norm1->backward(out);
        out = down3_conv1->backward(out);

        out = down3_pool->backward(out);


        out = Mat2d::sum(out, up2_out[1]);
        out = down2_relu2->backward(out);
        out = down2_norm2->backward(out);
        out = down2_conv2->backward(out);

        out = down2_relu1->backward(out);
        out = down2_norm1->backward(out);
        out = down2_conv1->backward(out);

        out = down2_pool->backward(out);


        out = Mat2d::sum(out, up3_out[1]);
        out = down1_relu2->backward(out);
        out = down1_norm2->backward(out);
        out = down1_conv2->backward(out);

        out = down1_relu1->backward(out);
        out = down1_norm1->backward(out);
        out = down1_conv1->backward(out);

        out = down1_pool->backward(out);


        out = Mat2d::sum(out, up4_out[1]);
        out = inc_relu2->backward(out);
        out = inc_norm2->backward(out);
        out = inc_conv2->backward(out);

        out = inc_relu1->backward(out);
        out = inc_norm1->backward(out);
        out = inc_conv1->backward(out);

        return out;
    }

    Mat2d::mat2d predict(const Mat2d::mat2d &X)
    {
        inc_norm1->eval();
        inc_norm2->eval();
        down1_norm1->eval();
        down1_norm2->eval();
        down2_norm1->eval();
        down2_norm2->eval();
        down3_norm1->eval();
        down3_norm2->eval();
        down4_norm1->eval();
        down4_norm2->eval();
        up1_norm1->eval();
        up1_norm2->eval();
        up2_norm1->eval();
        up2_norm2->eval();
        up3_norm1->eval();
        up3_norm2->eval();
        up4_norm1->eval();
        up4_norm2->eval();

        Mat2d::mat2d result = forward(X);

        inc_norm1->train();
        inc_norm2->train();
        down1_norm1->train();
        down1_norm2->train();
        down2_norm1->train();
        down2_norm2->train();
        down3_norm1->train();
        down3_norm2->train();
        down4_norm1->train();
        down4_norm2->train();
        up1_norm1->train();
        up1_norm2->train();
        up2_norm1->train();
        up2_norm2->train();
        up3_norm1->train();
        up3_norm2->train();
        up4_norm1->train();
        up4_norm2->train();

        return result;
    }

    std::vector<std::shared_ptr<Weights2d>> params()
    {
        std::vector<std::shared_ptr<Weights2d>> buf;
        std::vector<std::shared_ptr<Weights2d>> result;

        buf = inc_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = inc_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = inc_conv2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = inc_norm2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down1_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down1_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down1_conv2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down1_norm2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down2_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down2_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down2_conv2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down2_norm2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down3_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down3_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down3_conv2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down3_norm2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down4_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down4_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down4_conv2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down4_norm2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up1_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up1_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up1_conv2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up1_norm2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up2_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up2_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up2_conv2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up2_norm2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up3_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up3_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up3_conv2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up3_norm2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up4_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up4_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up4_conv2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up4_norm2->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = outc->params();
        result.insert(result.end(), buf.begin(), buf.end());


        return result;
    }

private:

    Mat2d::mat2d cat_channel(const Mat2d::mat2d &mat1, const Mat2d::mat2d &mat2)
    {
        Mat2d::size_t mat1_size = Mat2d::size(mat1);
        Mat2d::size_t mat2_size = Mat2d::size(mat2);

        if(mat1_size[0] != mat2_size[0] || mat1_size[1] != mat2_size[1] ||
           mat1_size[2] != mat2_size[2] || mat1_size[3] != mat2_size[3])
            throw std::invalid_argument("");
        if(mat1.empty() || mat2.empty())
            throw std::invalid_argument("");

        int batch_size = mat1_size[0];
        int channels = mat1_size[1];
        int height = mat1_size[2];
        int width = mat1_size[3];

        Mat2d::mat2d result = Mat2d::zeros(Mat2d::size_t(batch_size, channels*2, height, width));
        for(int b = 0; b < batch_size; b++)
            for(int c = 0; c < channels; c++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++)
                        result[b][c][h][w] = mat2[b][c][h][w];

        for(int b = 0; b < batch_size; b++)
            for(int c = channels; c < channels*2; c++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++)
                        result[b][c][h][w] = mat1[b][c-channels][h][w];

        return result;
    }

    std::vector<Mat2d::mat2d> uncat_channel(const Mat2d::mat2d &_mat)
    {
        if(_mat.empty())
            throw std::invalid_argument("");

        Mat2d::size_t mat_size = Mat2d::size(_mat);
        int batch_size = mat_size[0];
        int channels = mat_size[1];
        int height = mat_size[2];
        int width = mat_size[3];

        Mat2d::mat2d result1 = Mat2d::zeros(Mat2d::size_t(batch_size, channels/2, height, width));
        Mat2d::mat2d result2 = result1;

        for(int b = 0; b < batch_size; b++)
            for(int c = 0; c < channels/2; c++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++)
                        result2[b][c][h][w] = _mat[b][c][h][w];

        for(int b = 0; b < batch_size; b++)
            for(int c = channels/2; c < channels; c++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++)
                        result1[b][c-(channels/2)][h][w] = _mat[b][c][h][w];

        return std::vector<Mat2d::mat2d>{result1, result2};
    }

};


class UNet_predict_only
{

private:

    double reg;

    std::unique_ptr<Conv2d_fast> inc_conv1 = std::unique_ptr<Conv2d_fast>(new Conv2d_fast(1, 16, 3, 1));
    std::unique_ptr<BatchNorm2d> inc_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(16));
    std::unique_ptr<ReLU2d> inc_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<MaxPool2d> down1_pool = std::unique_ptr<MaxPool2d>(new MaxPool2d(2));

    std::unique_ptr<Conv2d_fast> down1_conv1 = std::unique_ptr<Conv2d_fast>(new Conv2d_fast(16, 32, 3, 0));
    std::unique_ptr<BatchNorm2d> down1_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(32));
    std::unique_ptr<ReLU2d> down1_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<MaxPool2d> down2_pool = std::unique_ptr<MaxPool2d>(new MaxPool2d(2));

    std::unique_ptr<Conv2d_fast> down2_conv1 = std::unique_ptr<Conv2d_fast>(new Conv2d_fast(32, 64, 3, 1));
    std::unique_ptr<BatchNorm2d> down2_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(64));
    std::unique_ptr<ReLU2d> down2_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<MaxPool2d> down3_pool = std::unique_ptr<MaxPool2d>(new MaxPool2d(2));

    std::unique_ptr<Conv2d_fast> down3_conv1 = std::unique_ptr<Conv2d_fast>(new Conv2d_fast(64, 128, 3, 1));
    std::unique_ptr<BatchNorm2d> down3_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(128));
    std::unique_ptr<ReLU2d> down3_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<MaxPool2d> down4_pool = std::unique_ptr<MaxPool2d>(new MaxPool2d(2));

    std::unique_ptr<Conv2d_fast> down4_conv1 = std::unique_ptr<Conv2d_fast>(new Conv2d_fast(128, 128, 3, 1));
    std::unique_ptr<BatchNorm2d> down4_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(128));
    std::unique_ptr<ReLU2d> down4_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());



    std::unique_ptr<Upsample2d> up1_upsample = std::unique_ptr<Upsample2d>(new Upsample2d(2));

    std::unique_ptr<Conv2d_fast> up1_conv1 = std::unique_ptr<Conv2d_fast>(new Conv2d_fast(256, 64, 3, 1));
    std::unique_ptr<BatchNorm2d> up1_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(64));
    std::unique_ptr<ReLU2d> up1_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<Upsample2d> up2_upsample = std::unique_ptr<Upsample2d>(new Upsample2d(2));

    std::unique_ptr<Conv2d_fast> up2_conv1 = std::unique_ptr<Conv2d_fast>(new Conv2d_fast(128, 32, 3, 1));
    std::unique_ptr<BatchNorm2d> up2_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(32));
    std::unique_ptr<ReLU2d> up2_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<Upsample2d> up3_upsample = std::unique_ptr<Upsample2d>(new Upsample2d(2));

    std::unique_ptr<Conv2d_fast> up3_conv1 = std::unique_ptr<Conv2d_fast>(new Conv2d_fast(64, 16, 3, 2));
    std::unique_ptr<BatchNorm2d> up3_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(16));
    std::unique_ptr<ReLU2d> up3_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<Upsample2d> up4_upsample = std::unique_ptr<Upsample2d>(new Upsample2d(2));

    std::unique_ptr<Conv2d_fast> up4_conv1 = std::unique_ptr<Conv2d_fast>(new Conv2d_fast(32, 16, 3, 1));
    std::unique_ptr<BatchNorm2d> up4_norm1 = std::unique_ptr<BatchNorm2d>(new BatchNorm2d(16));
    std::unique_ptr<ReLU2d> up4_relu1 = std::unique_ptr<ReLU2d>(new ReLU2d());


    std::unique_ptr<Conv2d_fast> outc = std::unique_ptr<Conv2d_fast>(new Conv2d_fast(16, 1, 1, 0));


public:

    UNet_predict_only(double reg = 0)
    {
        this->reg = reg;
    }

    double compute_loss_and_gradients(const Mat2d::mat2d &X, const Mat2d::mat2d &y)
    {
        //TODO
    }

    Mat2d::mat2d forward(const Mat2d::mat2d &X)
    {
        Mat2d::mat2d out = inc_conv1->forward(X);
        out = inc_norm1->forward(out);
        out = inc_relu1->forward(out);
        Mat2d::mat2d inc_out = out;


        out = down1_pool->forward(out);

        out = down1_conv1->forward(out);
        out = down1_norm1->forward(out);
        out = down1_relu1->forward(out);
        Mat2d::mat2d down1_out = out;


        out = down2_pool->forward(out);

        out = down2_conv1->forward(out);
        out = down2_norm1->forward(out);
        out = down2_relu1->forward(out);
        Mat2d::mat2d down2_out = out;


        out = down3_pool->forward(out);

        out = down3_conv1->forward(out);
        out = down3_norm1->forward(out);
        out = down3_relu1->forward(out);
        Mat2d::mat2d down3_out = out;


        out = down4_pool->forward(out);

        out = down4_conv1->forward(out);
        out = down4_norm1->forward(out);
        out = down4_relu1->forward(out);



        out = up1_upsample->forward(out);
        out = cat_channel(out, down3_out);

        out = up1_conv1->forward(out);
        out = up1_norm1->forward(out);
        out = up1_relu1->forward(out);


        out = up2_upsample->forward(out);
        out = cat_channel(out, down2_out);

        out = up2_conv1->forward(out);
        out = up2_norm1->forward(out);
        out = up2_relu1->forward(out);


        out = up3_upsample->forward(out);
        out = cat_channel(out, down1_out);

        out = up3_conv1->forward(out);
        out = up3_norm1->forward(out);
        out = up3_relu1->forward(out);


        out = up4_upsample->forward(out);
        out = cat_channel(out, inc_out);

        out = up4_conv1->forward(out);
        out = up4_norm1->forward(out);
        out = up4_relu1->forward(out);


        out = outc->forward(out);

        return out;
    }

    Mat2d::mat2d backward(const Mat2d::mat2d &d_out)
    {
       //TODO
    }

    Mat2d::mat2d predict(const Mat2d::mat2d &X)
    {
        inc_norm1->eval();
        down1_norm1->eval();
        down2_norm1->eval();
        down3_norm1->eval();
        down4_norm1->eval();
        up1_norm1->eval();
        up2_norm1->eval();
        up3_norm1->eval();
        up4_norm1->eval();

        Mat2d::mat2d result = forward(X);

        inc_norm1->train();
        down1_norm1->train();
        down2_norm1->train();
        down3_norm1->train();
        down4_norm1->train();
        up1_norm1->train();
        up2_norm1->train();
        up3_norm1->train();
        up4_norm1->train();

        return result;
    }


    void save()
    {
        std::vector<std::shared_ptr<Weights2d>> weights = params();
        Vec::vec buf;
        for(int i = 0; i < weights.size(); i++)
        {
            Mat2d::size_t w_size = Mat2d::size(weights[i]->value);
            for(int b = 0; b < w_size[0]; b++)
                for(int c = 0; c < w_size[1]; c++)
                    for(int h = 0; h < w_size[2]; h++)
                        for(int w = 0; w < w_size[3]; w++)
                            buf.push_back(weights[i]->value[b][c][h][w]);
        }
        
        std::ofstream file("unet_weights", std::ios::binary);
        file.write(reinterpret_cast<char*>(buf.data()), buf.size()*sizeof(double));
        file.close();
    }

    void load(std::string file_name = "unet_weights")
    {
        std::vector<std::shared_ptr<Weights2d>> weights = params();
        size_t weights_size = weights.size();
        size_t i, size;
        for(i = 0, size = 0; i < weights_size; ++i)
        {
            Mat2d::size_t w_size = Mat2d::size(weights[i]->value);
            size += w_size[0] * w_size[1] * w_size[2] * w_size[3];
        }
        std::vector<double> buf(size);

        std::ifstream file(file_name, std::ios::binary);
        file.read(reinterpret_cast<char*>(&buf.front()), buf.size()*sizeof(uint64_t));
        file.close();

        size_t it;
        for(i = 0, it = 0; i < weights_size; ++i)
        {
            Mat2d::size_t w_size = Mat2d::size(weights[i]->value);
            for(int b = 0; b < w_size[0]; ++b)
                for(int c = 0; c < w_size[1]; ++c)
                    for(int h = 0; h < w_size[2]; ++h)
                        for(int w = 0; w < w_size[3]; ++w, ++it)
                            weights[i]->value[b][c][h][w] = buf[it];
        }

        if(it != size)
            throw std::invalid_argument("");
    }

    void load_from_pytorch()
    {
        std::vector<std::shared_ptr<Weights2d>> weights = params();
        for(int i = 0; i < weights.size(); i++)
        {
            Mat2d::size_t w_size = Mat2d::size(weights[i]->value);
            std::ifstream file("w/model_weights_" + std::to_string(i), std::ios::in);
            int it = 0;
            for(int b = 0; b < w_size[0]; b++)
                for(int c = 0; c < w_size[1]; c++)
                    for(int h = 0; h < w_size[2]; h++)
                        for(int w = 0; w < w_size[3]; w++, it++)
                        {
                            double num = 0.0;
                            if(file >> num)
                                weights[i]->value[b][c][h][w] = num;
                            else
                                throw std::invalid_argument(std::to_string(i) + " " + 
                                                            std::to_string(w_size[0]*w_size[1]*w_size[2]*w_size[3]) + " " + 
                                                            std::to_string(it));
                        }
            if(it != w_size[0]*w_size[1]*w_size[2]*w_size[3])
                throw std::invalid_argument("");
            file.close();
        }
    }

    std::vector<std::shared_ptr<Weights2d>> params()
    {
        std::vector<std::shared_ptr<Weights2d>> buf;
        std::vector<std::shared_ptr<Weights2d>> result;

        buf = inc_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = inc_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down1_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down1_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down2_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down2_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down3_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down3_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down4_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = down4_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up1_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up1_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up2_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up2_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up3_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up3_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up4_conv1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = up4_norm1->params();
        result.insert(result.end(), buf.begin(), buf.end());

        buf = outc->params();
        result.insert(result.end(), buf.begin(), buf.end());


        return result;
    }

private:

    Mat2d::mat2d cat_channel(const Mat2d::mat2d &mat1, const Mat2d::mat2d &mat2)
    {
        Mat2d::size_t mat1_size = Mat2d::size(mat1);
        Mat2d::size_t mat2_size = Mat2d::size(mat2);

        if(mat1_size[0] != mat2_size[0] || mat1_size[1] != mat2_size[1] ||
           mat1_size[2] != mat2_size[2] || mat1_size[3] != mat2_size[3])
            throw std::invalid_argument("");
        if(mat1.empty() || mat2.empty())
            throw std::invalid_argument("");

        int batch_size = mat1_size[0];
        int channels = mat1_size[1];
        int height = mat1_size[2];
        int width = mat1_size[3];

        Mat2d::mat2d result = Mat2d::zeros(Mat2d::size_t(batch_size, channels*2, height, width));
        for(int b = 0; b < batch_size; b++)
            for(int c = 0; c < channels; c++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++)
                        result[b][c][h][w] = mat2[b][c][h][w];

        for(int b = 0; b < batch_size; b++)
            for(int c = channels; c < channels*2; c++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++)
                        result[b][c][h][w] = mat1[b][c-channels][h][w];

        return result;
    }

    std::vector<Mat2d::mat2d> uncat_channel(const Mat2d::mat2d &_mat)
    {
        if(_mat.empty())
            throw std::invalid_argument("");

        Mat2d::size_t mat_size = Mat2d::size(_mat);
        int batch_size = mat_size[0];
        int channels = mat_size[1];
        int height = mat_size[2];
        int width = mat_size[3];

        Mat2d::mat2d result1 = Mat2d::zeros(Mat2d::size_t(batch_size, channels/2, height, width));
        Mat2d::mat2d result2 = result1;

        for(int b = 0; b < batch_size; b++)
            for(int c = 0; c < channels/2; c++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++)
                        result2[b][c][h][w] = _mat[b][c][h][w];

        for(int b = 0; b < batch_size; b++)
            for(int c = channels/2; c < channels; c++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++)
                        result1[b][c-(channels/2)][h][w] = _mat[b][c][h][w];

        return std::vector<Mat2d::mat2d>{result1, result2};
    }

};


#endif