#ifndef LAYERS_H
#define LAYERS_H

#include <iostream>
#include <vector>
#include <map>
#include <memory>
#include "mat.h"
#include "rand.h"
#include "computational_graph.h"
#include "filter.h"
#include <thread>


class Loss
{

public:

    double value;
    Mat::mat grad;

    Loss() {}

    Loss(double value, const Mat::mat &grad)
    {
        this->value = value;
        this->grad = Mat::mat(grad);
    }
};


Loss l2_regularization(const Mat::mat &W, double reg_strength)
{
    double loss = reg_strength * Mat::sum(Mat::pow(W, 2));
    Mat::mat grad(Mat::prod(W, reg_strength * 2.0));
    return Loss(loss, grad);
}


Loss mean_squared_error(const Mat::mat &predictions, const Mat::mat &target_index)
{
    int N = predictions.size();
    Mat::mat error(Mat::diff(predictions, target_index));

    double loss = Mat::sum(Mat::pow(error, 2)) / N;
    Mat::mat grad(Mat::prod(error, 2.0 / N));

    return Loss(loss, grad);
}


/*
double distance_loss(const Mat::mat &predictions, const Mat::mat &target_index)
{
    int N = predictions.size();
    double loss = 0;
    for(int i = 0; i < N; i++)
    {
        double distance_max = 0;
        for(int j = 0; j < predictions[i].size(); j += 2)
        {
            double distance = std::sqrt(std::pow(predictions[i][j] - target_index[i][j], 2) + 
                                        std::pow(predictions[i][j+1] - target_index[i][j+1], 2));
            if(distance_max < distance)
                distance_max = distance;
        }
        loss += distance_max;
    }
    loss /= N;
    return loss;
}
*/


Loss distance_loss(const Mat::mat &predictions, const Mat::mat &target_index)
{
    Mat::mat error(Mat::diff(predictions, target_index));
    cg::Distance distance;
    double loss = distance.forward(error);
    Mat::mat grad = distance.backward(1.0);
    return Loss(loss, grad);
}


class Weights
{

public:

    //TODO: private vars
    Mat::mat value;
    Mat::mat grad;

    //TODO: delete empty constructor
    Weights() {}

    Weights(const Mat::mat &value)
    {
        //TODO: check: if value is empty
        this->value = Mat::mat(value);
        reset_grad();
    }

    void reset_grad()
    {
        grad = Mat::zeros(value.size(), value[0].size());
    }
};


class Layer
{

public:

    virtual Mat::mat forward(const Mat::mat &X) = 0;
    virtual Mat::mat backward(const Mat::mat &d_out) = 0;
    virtual std::vector<std::shared_ptr<Weights>> params() = 0;

};


class ReLULayer : public Layer
{

private:

    Mat::mat X;

public:

    Mat::mat forward(const Mat::mat &X)
    {
        this->X = Mat::mat(X);
        Mat::mat result(X);
        for(int i = 0; i < X.size(); i++)
            for(int j = 0; j < X[i].size(); j++)
                if(X[i][j] <= 0)
                    result[i][j] = 0;
        return result;
    }

    Mat::mat backward(const Mat::mat &d_out)
    {
        Mat::mat result(d_out);
        for(int i = 0; i < X.size(); i++)
            for(int j = 0; j < X[i].size(); j++)
                if(X[i][j] <= 0)
                    result[i][j] = 0;
        return result;
    }

    std::vector<std::shared_ptr<Weights>> params()
    {
        return std::vector<std::shared_ptr<Weights>>();
    }
};


class LeakyReLULayer : public Layer
{

private:

    Mat::mat X;
    double alpha;

public:

    LeakyReLULayer(double alpha = 0.01)
    {
        this->alpha = alpha;
    }

    Mat::mat forward(const Mat::mat &X)
    {
        this->X = Mat::mat(X);
        Mat::mat result(X);
        for(int i = 0; i < X.size(); i++)
            for(int j = 0; j < X[i].size(); j++)
                if(X[i][j] <= 0)
                    result[i][j] *= alpha;
        return result;
    }

    Mat::mat backward(const Mat::mat &d_out)
    {
        Mat::mat result(d_out);
        for(int i = 0; i < X.size(); i++)
            for(int j = 0; j < X[i].size(); j++)
                if(X[i][j] <= 0)
                    result[i][j] *= alpha;
        return result;
    }

    std::vector<std::shared_ptr<Weights>> params()
    {
        return std::vector<std::shared_ptr<Weights>>();
    }
};


class FullyConnectedLayer : public Layer
{

private:

    std::shared_ptr<Weights> W;
    std::shared_ptr<Weights> B;
    Mat::mat X;

public:

    FullyConnectedLayer(int n_input, int n_output):
        //W(new Weights(Mat::zeros(n_input, n_output))),
        //B(new Weights(Mat::zeros(1, n_output)))
        W(new Weights(Mat::prod(Mat::randn(n_input, n_output), 0.001))),
        B(new Weights(Mat::prod(Mat::randn(1, n_output), 0.001)))
        {}

    Mat::mat forward(const Mat::mat &X)
    {
        this->X = Mat::mat(X);
        return Mat::sum(Mat::dot(X, W->value), B->value);
    }

    Mat::mat backward(const Mat::mat &d_out)
    {
        W->grad = Mat::dot(Mat::transpose(X), d_out);
        B->grad = Mat::dot(Mat::ones(1, X.size()), d_out);
        return Mat::dot(d_out, Mat::transpose(W->value));
    }

    std::vector<std::shared_ptr<Weights>> params()
    {
        return std::vector<std::shared_ptr<Weights>>{W, B};
    }
};



class Loss2d
{

public:

    double value;
    Mat2d::mat2d grad;

    Loss2d() {}

    Loss2d(double value, const Mat2d::mat2d &grad)
    {
        this->value = value;
        this->grad = Mat2d::mat2d(grad);
    }
};


Loss2d mean_squared_error(const Mat2d::mat2d &predictions, const Mat2d::mat2d &target_index)
{
    int N = predictions.size();
    Mat2d::mat2d error(Mat2d::diff(predictions, target_index));

    double loss = Mat2d::sum(Mat2d::pow(error, 2)) / (double)N;
    Mat2d::mat2d grad(Mat2d::prod(error, 2.0 / (double)N));

    return Loss2d(loss, grad);
}


Loss2d l2_regularization(const Mat2d::mat2d &W, double reg_strength)
{
    double loss = reg_strength * Mat2d::sum(Mat2d::pow(W, 2));
    Mat2d::mat2d grad(Mat2d::prod(W, reg_strength * 2.0));
    return Loss2d(loss, grad);
}


class Weights2d
{

public:

    Mat2d::mat2d value;
    Mat2d::mat2d grad;

    Weights2d() {}

    Weights2d(const Mat2d::mat2d &value)
    {
        this->value = value;
    }

    Weights2d(Mat2d::mat2d &value)
    {
        this->value = value;
    }

    void reset_grad()
    {
        grad = Mat2d::zeros(Mat2d::size(value));
    }
};


class Layer2d
{

protected:

    bool _eval = false;

public:

    virtual Mat2d::mat2d forward(const Mat2d::mat2d &X) = 0;
    virtual Mat2d::mat2d backward(const Mat2d::mat2d &d_out) = 0;
    virtual std::vector<std::shared_ptr<Weights2d>> params() = 0;

    void train()
    {
        _eval = false;
    }

    void eval()
    {
        _eval = true;
    }

};


class ReLU2d : public Layer2d
{

private:

    Mat2d::mat2d X;

public:

    Mat2d::mat2d forward(const Mat2d::mat2d &X)
    {
        this->X = X;

        Mat2d::size_t x_size = Mat2d::size(this->X);
        int batch_size = x_size[0];
        int channels = x_size[1];
        int height = x_size[2];
        int width = x_size[3];

        Mat2d::mat2d result = Mat2d::mat2d(X);
        for(int b = 0; b < batch_size; b++)
            for(int c = 0; c < channels; c++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++)
                        if(result[b][c][h][w] <= 0)
                            result[b][c][h][w] = 0;

        return result;
    }

    Mat2d::mat2d backward(const Mat2d::mat2d &d_out)
    {
        Mat2d::mat2d d_input = Mat2d::mat2d(d_out);

        Mat2d::size_t x_size = Mat2d::size(this->X);
        int batch_size = x_size[0];
        int channels = x_size[1];
        int height = x_size[2];
        int width = x_size[3];

        for(int b = 0; b < batch_size; b++)
            for(int c = 0; c < channels; c++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++)
                        if(this->X[b][c][h][w] <= 0)
                            d_input[b][c][h][w] = 0;

        return d_input;
    }

    std::vector<std::shared_ptr<Weights2d>> params()
    {
        return std::vector<std::shared_ptr<Weights2d>>();
    }
};


class Conv2d : public Layer2d
{

public:

    std::shared_ptr<Weights2d> W;
    std::shared_ptr<Weights2d> B;

private:

    int in_channels;
    int out_channels;
    int filter_size;
    int padding;
    // std::shared_ptr<Weights2d> W;
    // std::shared_ptr<Weights2d> B;
    Mat2d::mat2d X;


public:

    Conv2d(int in_channels, int out_channels, int filter_size, int padding = 0):
        in_channels(in_channels),
        out_channels(out_channels),
        filter_size(filter_size),
        padding(padding),
        W(new Weights2d(Mat2d::randn(Mat2d::size_t(in_channels, filter_size, filter_size, out_channels)))),
        B(new Weights2d(Mat2d::randn(Mat2d::size_t(1, 1, 1, out_channels))))
        {
        }

    Mat2d::mat2d forward(const Mat2d::mat2d &X)
    {
        if(padding != 0)
            this->X = add_padding(X, padding);
        else
            this->X = Mat2d::mat2d(X);

        Mat2d::size_t x_size = Mat2d::size(this->X);
        int batch_size = x_size[0];
        int channels = x_size[1];
        int height = x_size[2];
        int width = x_size[3];

        int out_height = (height - filter_size) + 1;
        int out_width = (width - filter_size) + 1;
        Mat2d::mat2d result = Mat2d::zeros(Mat2d::size_t(batch_size, out_channels, out_height, out_width));

        Mat::mat W;
        for(int i = 0; i < in_channels; i++)
            for(int j = 0; j < filter_size; j++)
                for(int k = 0; k < filter_size; k++)
                    W.push_back(this->W->value[i][j][k]);

        Mat::mat B{this->B->value[0][0][0]};

        for(int h_out = 0; h_out < out_height; h_out++)
            for(int w_out = 0; w_out < out_width; w_out++)
            {
                Mat::mat X_slice;
                for(int b = 0; b < batch_size; b++)
                {
                    X_slice.push_back(Vec::vec());
                    for(int c = 0; c < channels; c++)
                    {
                        for(int h = h_out; h < h_out+filter_size; h++)
                        {
                            auto first = this->X[b][c][h].begin() + w_out;
                            auto last = this->X[b][c][h].begin() + (w_out + filter_size);
                            X_slice[b].insert(X_slice[b].end(), first, last);
                        }
                    }
                }

                Mat::mat _result = Mat::sum(Mat::dot(X_slice, W), B);
                for(int b = 0; b < batch_size; b++)
                    for(int c_out = 0; c_out < out_channels; c_out++)
                        result[b][c_out][h_out][w_out] = _result[b][c_out];
            }

        return result;
    }

    Mat2d::mat2d backward(const Mat2d::mat2d &d_out)
    {
        Mat2d::size_t x_size = Mat2d::size(this->X);
        int batch_size = x_size[0];
        int channels = x_size[1];
        int height = x_size[2];
        int width = x_size[3];

        Mat2d::size_t out_size = Mat2d::size(d_out);
        int out_height = out_size[2];
        int out_width = out_size[3];

        Mat::mat W;
        for(int i = 0; i < in_channels; i++)
            for(int j = 0; j < filter_size; j++)
                for(int k = 0; k < filter_size; k++)
                    W.push_back(this->W->value[i][j][k]);

        Mat2d::mat2d d_input = Mat2d::zeros(x_size);
        for(int h_out = 0; h_out < out_height; h_out++)
            for(int w_out = 0; w_out < out_width; w_out++)
            {
                Mat::mat X_slice;
                Mat::mat d_out_slice;
                for(int b = 0; b < batch_size; b++)
                {
                    X_slice.push_back(Vec::vec());
                    for(int c = 0; c < channels; c++)
                    {
                        for(int h = h_out; h < h_out+filter_size; h++)
                        {
                            auto first = this->X[b][c][h].begin() + w_out;
                            auto last = this->X[b][c][h].begin() + (w_out + filter_size);
                            X_slice[b].insert(X_slice[b].end(), first, last);
                        }
                    }

                    d_out_slice.push_back(Vec::vec());
                    for(int c_out = 0; c_out < out_channels; c_out++)
                        d_out_slice[b].push_back(d_out[b][c_out][h_out][w_out]);
                }

                Mat::mat W_grad = Mat::dot(Mat::transpose(X_slice), d_out_slice);
                for(int c_in = 0, i = 0; c_in < in_channels; c_in++)
                    for(int fs1 = 0; fs1 < filter_size; fs1++)
                        for(int fs2 = 0; fs2 < filter_size; fs2++, i++)
                            this->W->grad[c_in][fs1][fs2] = Vec::sum(this->W->grad[c_in][fs1][fs2], W_grad[i]);

                Mat::mat X_grad = Mat::dot(d_out_slice, Mat::transpose(W));
                for(int b = 0; b < batch_size; b++)
                    for(int c = 0, i = 0; c < channels; c++)
                        for(int h = h_out; h < h_out+filter_size; h++)
                            for(int w = w_out; w < w_out+filter_size; w++, i++)
                                d_input[b][c][h][w] += X_grad[b][i];

                Mat::mat B_grad = Mat::dot(Mat::ones(1, batch_size), d_out_slice);
                this->B->grad[0][0][0] = Vec::sum(this->B->grad[0][0][0], B_grad[0]);
            }
      
        if(padding != 0)
            return del_padding(d_input, padding);

        return d_input;
    }

    std::vector<std::shared_ptr<Weights2d>> params()
    {
        return std::vector<std::shared_ptr<Weights2d>>{W, B};
    }

private:

    Mat2d::mat2d add_padding(const Mat2d::mat2d &mat, int padding)
    {
        Mat2d::size_t mat_size = Mat2d::size(mat);
        int batch_size = mat_size[0];
        int channels = mat_size[1];
        int height = mat_size[2];
        int width = mat_size[3];

        Mat2d::mat2d result = Mat2d::zeros(Mat2d::size_t(batch_size,
                                                         channels,
                                                         height + (padding * 2), 
                                                         width + (padding * 2)));
        for(int i = 0; i < batch_size; i++)
            for(int j = 0; j < channels; j++)
                for(int k = 0; k < height; k++)
                    for(int l = 0; l < width; l++)
                        result[i][j][k+padding][l+padding] = mat[i][j][k][l];
        return result;
    }

    Mat2d::mat2d del_padding(const Mat2d::mat2d &mat, int padding)
    {
        Mat2d::size_t mat_size = Mat2d::size(mat);
        int batch_size = mat_size[0];
        int channels = mat_size[1];
        int height = mat_size[2];
        int width = mat_size[3];

        Mat2d::mat2d result = Mat2d::zeros(Mat2d::size_t(batch_size,
                                                         channels,
                                                         height - (padding * 2), 
                                                         width - (padding * 2)));
        for(int i = 0; i < batch_size; i++)
            for(int j = 0; j < channels; j++)
                for(int k = 0; k < height - (padding * 2); k++)
                    for(int l = 0; l < width - (padding * 2); l++)
                        result[i][j][k][l] = mat[i][j][k+padding][l+padding];
        return result;
    }

};


class MaxPool2d : public Layer2d
{

private:

    int pool_size;
    int stride;
    Mat2d::mat2d X;

public:

    MaxPool2d(int pool_size)
    {
        this->pool_size = pool_size;
        this->stride = pool_size;
    }

    MaxPool2d(int pool_size, int stride)
    {
        this->pool_size = pool_size;
        this->stride = stride;
    }

    Mat2d::mat2d forward(const Mat2d::mat2d &X)
    {
        this->X = X;

        Mat2d::size_t x_size = Mat2d::size(this->X);
        int batch_size = x_size[0];
        int channels = x_size[1];
        int height = x_size[2];
        int width = x_size[3];

        int out_height = ((height - pool_size) / stride) + 1;
        int out_width = ((width - pool_size) / stride) + 1;
        Mat2d::mat2d result = Mat2d::zeros(Mat2d::size_t(batch_size, channels, out_height, out_width));

        for(int h_out = 0; h_out < out_height; h_out++)
            for(int w_out = 0; w_out < out_width; w_out++)
                for(int b = 0; b < batch_size; b++)
                    for(int c = 0; c < channels; c++)
                    {
                        double max = Utils::double_min;
                        for(int h = h_out*stride; h < h_out*stride+pool_size; h++)
                            for(int w = w_out*stride; w < w_out*stride+pool_size; w++)
                            {
                                if(this->X[b][c][h][w] > max)
                                    max = this->X[b][c][h][w];
                            }
                        result[b][c][h_out][w_out] = max;
                    }

        return result;
    }

    Mat2d::mat2d backward(const Mat2d::mat2d &d_out)
    {
        Mat2d::size_t x_size = Mat2d::size(this->X);
        int batch_size = x_size[0];
        int channels = x_size[1];

        Mat2d::size_t out_size = Mat2d::size(d_out);
        int out_height = out_size[2];
        int out_width = out_size[3];

        Mat2d::mat2d d_input = Mat2d::zeros(x_size);

        for(int h_out = 0; h_out < out_height; h_out++)
            for(int w_out = 0; w_out < out_width; w_out++)
                for(int b = 0; b < batch_size; b++)
                    for(int c = 0; c < channels; c++)
                    {
                        double max = Utils::double_min;
                        for(int h = h_out*stride; h < h_out*stride+pool_size; h++)
                            for(int w = w_out*stride; w < w_out*stride+pool_size; w++)
                            {
                                if(this->X[b][c][h][w] > max)
                                    max = this->X[b][c][h][w];
                            }

                        int max_count = 0;
                        for(int h = h_out*stride; h < h_out*stride+pool_size; h++)
                            for(int w = w_out*stride; w < w_out*stride+pool_size; w++)
                                if(this->X[b][c][h][w] == max)
                                {
                                    d_input[b][c][h][w] = 1;
                                    max_count++;
                                }

                        for(int h = h_out*stride; h < h_out*stride+pool_size; h++)
                            for(int w = w_out*stride; w < w_out*stride+pool_size; w++)
                                if(this->X[b][c][h][w] == max)
                                {
                                    d_input[b][c][h][w] /= max_count;
                                    d_input[b][c][h][w] *= d_out[b][c][h_out][w_out];
                                }
                    }

        return d_input;

    }

    std::vector<std::shared_ptr<Weights2d>> params()
    {
        return std::vector<std::shared_ptr<Weights2d>>();
    }

};


// upsampling: nearest interpolation
class Upsample2d : public Layer2d
{

private:

    int scale_factor;
    Mat2d::size_t X_size;

public:

    Upsample2d(int scale_factor)
    {
        this->scale_factor = scale_factor;
    }

    Mat2d::mat2d forward(const Mat2d::mat2d &X)
    {
        X_size = Mat2d::size(X);

        int batch_size = X_size[0];
        int channels = X_size[1];
        int height = X_size[2];
        int width = X_size[3];

        int out_height = height * scale_factor;
        int out_width = width * scale_factor;
        Mat2d::mat2d result = Mat2d::zeros(Mat2d::size_t(batch_size, channels, out_height, out_width));

        for(int b = 0; b < batch_size; b++)
            for(int c = 0; c < channels; c++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++)
                        for(int h_out = h*scale_factor; h_out < h*scale_factor+scale_factor; h_out++)
                            for(int w_out = w*scale_factor; w_out < w*scale_factor+scale_factor; w_out++)
                                    result[b][c][h_out][w_out] = X[b][c][h][w];

        return result;
    }

    Mat2d::mat2d backward(const Mat2d::mat2d &d_out)
    {
        int batch_size = X_size[0];
        int channels = X_size[1];
        int height = X_size[2];
        int width = X_size[3];

        Mat2d::size_t out_size = Mat2d::size(d_out);
        int out_height = out_size[2];
        int out_width = out_size[3];

        Mat2d::mat2d d_input = Mat2d::zeros(X_size);

        for(int b = 0; b < batch_size; b++)
            for(int c = 0; c < channels; c++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++)
                        for(int h_out = h*scale_factor; h_out < h*scale_factor+scale_factor; h_out++)
                            for(int w_out = w*scale_factor; w_out < w*scale_factor+scale_factor; w_out++)
                                d_input[b][c][h][w] += d_out[b][c][h_out][w_out];

        return d_input;
    }

    std::vector<std::shared_ptr<Weights2d>> params()
    {
        return std::vector<std::shared_ptr<Weights2d>>();
    }   
     
};


class BatchNorm2d : public Layer2d
{

private:

    int num_features;
    double eps;
    double momentum;
    Mat2d::mat2d x_norm;
    std::shared_ptr<Weights2d> gamma;
    std::shared_ptr<Weights2d> beta;
    std::shared_ptr<Weights2d> eval_mean_channels;
    std::shared_ptr<Weights2d> eval_var_channels;

    Vec::vec std_channels;

public:

    BatchNorm2d(int num_features, double eps = 0.00001, double momentum = 0.1):
        num_features(num_features),
        eps(eps),
        momentum(momentum),
        gamma(new Weights2d(Mat2d::ones(Mat2d::size_t(1, 1, 1, num_features)))),
        beta(new Weights2d(Mat2d::zeros(Mat2d::size_t(1, 1, 1, num_features)))),
        eval_mean_channels(new Weights2d(Mat2d::zeros(Mat2d::size_t(1, 1, 1, num_features)))),
        eval_var_channels(new Weights2d(Mat2d::ones(Mat2d::size_t(1, 1, 1, num_features))))
        {}

    Mat2d::mat2d forward(const Mat2d::mat2d &X)
    {
        x_norm = X;

        Mat2d::size_t x_size = Mat2d::size(x_norm);
        int batch_size = x_size[0];
        int channels = x_size[1];
        int height = x_size[2];
        int width = x_size[3];

        if(!_eval)
        {
            std_channels = Vec::zeros(channels);

            for(int c = 0; c < channels; c++)
            {
                double mean = 0;
                for(int b = 0; b < batch_size; b++)
                    for(int h = 0; h < height; h++)
                        for(int w = 0; w < width; w++)
                            mean += x_norm[b][c][h][w];
                mean /= batch_size;

                double var = 0;
                for(int b = 0; b < batch_size; b++)
                    for(int h = 0; h < height; h++)
                        for(int w = 0; w < width; w++)
                            var += std::pow(x_norm[b][c][h][w] - mean, 2);
                var /= batch_size;
                double std = std::sqrt(var + eps);
                std_channels[c] = std;

                for(int b = 0; b < batch_size; b++)
                    for(int h = 0; h < height; h++)
                        for(int w = 0; w < width; w++)
                        {
                            x_norm[b][c][h][w] = (x_norm[b][c][h][w] - mean) / std;
                            x_norm[b][c][h][w] = x_norm[b][c][h][w] * gamma->value[0][0][0][c] + beta->value[0][0][0][c];
                        }

                eval_mean_channels->value[0][0][0][c] = (1 - momentum) * eval_mean_channels->value[0][0][0][c] + momentum * mean;
                eval_var_channels->value[0][0][0][c] = (1 - momentum) * eval_var_channels->value[0][0][0][c] + momentum * var;
            }
        }
        else
        {
            for(int c = 0; c < channels; c++)
            {
                double std = std::sqrt(eval_var_channels->value[0][0][0][c] + eps);
                double mean = eval_mean_channels->value[0][0][0][c];
                for(int b = 0; b < batch_size; b++)
                    for(int h = 0; h < height; h++)
                        for(int w = 0; w < width; w++)
                        {
                            x_norm[b][c][h][w] = (x_norm[b][c][h][w] - mean) / std;
                            x_norm[b][c][h][w] = x_norm[b][c][h][w] * gamma->value[0][0][0][c] + beta->value[0][0][0][c];
                        }
            }
        }

        return x_norm;
    }

    Mat2d::mat2d backward(const Mat2d::mat2d &d_out)
    {
        Mat2d::size_t x_size = Mat2d::size(x_norm);
        int batch_size = x_size[0];
        int channels = x_size[1];
        int height = x_size[2];
        int width = x_size[3];

        Mat2d::mat2d d_input = Mat2d::zeros(x_size);

        for(int c = 0; c < channels; c++)
        {
            double dbeta = 0;
            double dgamma = 0;
            for(int b = 0; b < batch_size; b++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++)
                    {
                        dbeta += d_out[b][c][h][w];
                        dgamma += d_out[b][c][h][w] * x_norm[b][c][h][w];
                        d_input[b][c][h][w] = d_out[b][c][h][w] * gamma->value[0][0][0][c];
                    }
            beta->grad[0][0][0][c] = dbeta;
            gamma->grad[0][0][0][c] = dgamma;
        }

        for(int c = 0; c < channels; c++)
        {
            double dx_norm_sum = 0;
            double buf = 0;
            for(int b = 0; b < batch_size; b++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++)
                    {
                        dx_norm_sum += d_input[b][c][h][w];
                        buf += d_input[b][c][h][w] * x_norm[b][c][h][w];
                    }

            for(int b = 0; b < batch_size; b++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++)
                    {
                        d_input[b][c][h][w] = batch_size * d_input[b][c][h][w] - 
                                              dx_norm_sum - x_norm[b][c][h][w] * buf;
                        d_input[b][c][h][w] /= batch_size * std_channels[c];
                    }
        }

        return d_input;
    }

    std::vector<std::shared_ptr<Weights2d>> params()
    {
        return std::vector<std::shared_ptr<Weights2d>>{gamma, beta, eval_mean_channels, eval_var_channels};
    }   

};


class Conv2d_thread : public Layer2d
{

private:

    int in_channels;
    int out_channels;
    int filter_size;
    int padding;
    std::shared_ptr<Weights2d> W;
    std::shared_ptr<Weights2d> B;
    Mat2d::mat2d X;

public:

    Conv2d_thread(int in_channels, int out_channels, int filter_size, int padding = 0):
        in_channels(in_channels),
        out_channels(out_channels),
        filter_size(filter_size),
        padding(padding)//,
        // W(new Weights2d(Mat2d::randn(Mat2d::size_t(out_channels, in_channels, filter_size, filter_size)))),
        // B(new Weights2d(Mat2d::randn(Mat2d::size_t(1, 1, 1, out_channels))))
        {
            W = std::shared_ptr<Weights2d>(new Weights2d(Mat2d::init(Mat2d::size_t(out_channels, in_channels, filter_size, filter_size))));
            B = std::shared_ptr<Weights2d>(new Weights2d(Mat2d::init(Mat2d::size_t(1, 1, 1, out_channels))));
        }

    void conv(Mat::mat &img, Mat::mat &kernel, Mat::mat &result)
    {
        int filter_size = kernel.size();
        int img_size = img.size();
        int out_N = result.size();
        
        for(int h_out = 0; h_out < out_N; h_out++)
            for(int w_out = 0; w_out < out_N; w_out++)
                for(int h = h_out, i = 0; h < h_out+filter_size; h++, i++)
                    for(int w = w_out, j = 0; w < w_out+filter_size; w++, j++)
                        result[h_out][w_out] += kernel[i][j] * img[h][w];
    }

    void forward_thread(std::vector<Mat::mat> &__X, std::vector<Mat::mat> &__W, Mat::mat &result, double in_channels, double out_h, double out_w, double bias)
    {
        Mat::mat _result = Mat::init(out_h, out_w, bias);
        for(int c_in = 0; c_in < in_channels; c_in++)
            conv(__X[c_in], __W[c_in], _result);
        result = _result;
    }


    Mat2d::mat2d forward(const Mat2d::mat2d &X)
    {
        if(padding != 0)
            this->X = add_padding(X, padding);
        else
            this->X = X;

        Mat2d::size_t x_size = Mat2d::size(this->X);
        int batch_size = x_size[0];
        int channels = x_size[1];
        int height = x_size[2];
        int width = x_size[3];

        int out_height = (height - filter_size) + 1;
        int out_width = (width - filter_size) + 1;
        Mat2d::mat2d result = Mat2d::zeros(Mat2d::size_t(batch_size, out_channels, out_height, out_width));

        std::vector<std::thread> th(batch_size*out_channels);

// std::cout << in_channels << " " << out_channels << "\n";
// auto begin = std::chrono::steady_clock::now();

        for(int b = 0, i = 0; b < batch_size; b++)
            for(int c_out = 0; c_out < out_channels; c_out++, i++)
                th[i] = std::thread(&Conv2d_thread::forward_thread, this, std::ref(this->X[b]), std::ref(this->W->value[c_out]), std::ref(result[b][c_out]), in_channels, out_height, out_width, this->B->value[0][0][0][c_out]);

        for(int i = 0; i < th.size(); i++)
            th[i].join();

// std::cout << "[time]: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count() << " ms" << std::endl;

        return result;
    }

    Mat2d::mat2d backward(const Mat2d::mat2d &d_out)
    {
        
    }

    std::vector<std::shared_ptr<Weights2d>> params()
    {
        return std::vector<std::shared_ptr<Weights2d>>{W, B};
    }

private:

    Mat2d::mat2d add_padding(const Mat2d::mat2d &mat, int padding)
    {
        Mat2d::size_t mat_size = Mat2d::size(mat);
        int batch_size = mat_size[0];
        int channels = mat_size[1];
        int height = mat_size[2];
        int width = mat_size[3];

        Mat2d::mat2d result = Mat2d::zeros(Mat2d::size_t(batch_size,
                                                         channels,
                                                         height + (padding * 2), 
                                                         width + (padding * 2)));
        for(int i = 0; i < batch_size; i++)
            for(int j = 0; j < channels; j++)
                for(int k = 0; k < height; k++)
                    for(int l = 0; l < width; l++)
                        result[i][j][k+padding][l+padding] = mat[i][j][k][l];
        return result;
    }
};


class Conv2d_fast : public Layer2d
{

private:

    int in_channels;
    int out_channels;
    int filter_size;
    int padding;
    std::shared_ptr<Weights2d> W;
    std::shared_ptr<Weights2d> B;
    Mat2d::mat2d X;


public:

    Conv2d_fast(int in_channels, int out_channels, int filter_size, int padding = 0):
        in_channels(in_channels),
        out_channels(out_channels),
        filter_size(filter_size),
        padding(padding)//,
        // W(new Weights2d(Mat2d::randn(Mat2d::size_t(out_channels, in_channels, filter_size, filter_size)))),
        // B(new Weights2d(Mat2d::randn(Mat2d::size_t(1, 1, 1, out_channels))))
        {
            W = std::shared_ptr<Weights2d>(new Weights2d(Mat2d::init(Mat2d::size_t(out_channels, in_channels, filter_size, filter_size))));
            B = std::shared_ptr<Weights2d>(new Weights2d(Mat2d::init(Mat2d::size_t(1, 1, 1, out_channels))));
        }

    Mat2d::mat2d forward(const Mat2d::mat2d &X)
    {
        if(padding != 0)
            this->X = add_padding(X, padding);
        else
            this->X = Mat2d::mat2d(X);

        Mat2d::size_t x_size = Mat2d::size(this->X);
        int batch_size = x_size[0];
        int channels = x_size[1];
        int height = x_size[2];
        int width = x_size[3];

        int out_height = (height - filter_size) + 1;
        int out_width = (width - filter_size) + 1;
        Mat2d::mat2d result = Mat2d::init(Mat2d::size_t(batch_size, out_channels, out_height, out_width));

        //X[bach_size][in_channels][height][width]
        //W[out_channels][in_channels][filter_size][filter_size]
        //result[batch_size][out_channels][out_height][out_width]
        double* _result = new double[batch_size*out_channels*out_height*out_width];
        double* _X = new double[batch_size*channels*height*width];
        double* _W = new double[out_channels*in_channels*filter_size*filter_size];

        for(int b = 0, i = 0; b < batch_size; b++)
            for(int out_c = 0; out_c < out_channels; out_c++)
                for(int out_h = 0; out_h < out_height; out_h++)
                    for(int out_w = 0; out_w < out_width; out_w++, i++)
                        *(_result + i) = this->B->value[0][0][0][out_c];

        for(int b = 0, i = 0; b < batch_size; b++)
            for(int c = 0; c < channels; c++)
                for(int h = 0; h < height; h++)
                    for(int w = 0; w < width; w++, i++)
                        *(_X + i) = this->X[b][c][h][w];

        for(int out_c = 0, i = 0; out_c < out_channels; out_c++)
            for(int in_c = 0; in_c < in_channels; in_c++)
                for(int f1 = 0; f1 < filter_size; f1++)
                    for(int f2 = 0; f2 < filter_size; f2++, i++)
                        *(_W + i) = this->W->value[out_c][in_c][f1][f2];


// std::cout << in_channels << " " << out_channels << "\n";
// auto begin = std::chrono::steady_clock::now();

        size_t b_x_step = channels*height*width;
        size_t b_r_max = batch_size*out_channels*out_height*out_width;
        size_t b_r_step = out_channels*out_height*out_width;

        size_t out_c_w_max = out_channels*channels*filter_size*filter_size;
        size_t out_c_w_step = channels*filter_size*filter_size;
        size_t out_c_r_step = out_height*out_width;

        size_t in_c_x_step = height*width;
        size_t in_c_w_max = channels*filter_size*filter_size;
        size_t in_c_w_step = filter_size*filter_size;

        size_t h_x_step = width;
        size_t h_r_max = out_height*out_width;
        size_t h_r_step = out_height;

        size_t b_x, b_r, out_c_w, out_c_r, in_c_x, in_c_w, h_x, h_r, w_x, f1, f2;
        register double res;

        for(b_x = 0, b_r = 0; b_r < b_r_max; b_x += b_x_step, b_r += b_r_step)
            for(out_c_w = 0, out_c_r = 0; out_c_w < out_c_w_max; out_c_w += out_c_w_step, out_c_r += out_c_r_step)
                for(in_c_x = 0, in_c_w = 0; in_c_w < in_c_w_max; in_c_x += in_c_x_step, in_c_w += in_c_w_step)
                    for(h_x = 0, h_r = 0; h_r < h_r_max; h_x += h_x_step, h_r += h_r_step)
                        for(w_x = 0; w_x < out_width; ++w_x)
                        {
                            res = 0.0;
                            for(f1 = 0; f1 < filter_size; ++f1)
                                for(f2 = 0; f2 < filter_size; ++f2)
                                    res += (*(_X + b_x + in_c_x + (h_x + (f1 * h_x_step)) + (w_x + f2))) * (*(_W + out_c_w + in_c_w + (filter_size * f1) + f2));
                            *(_result + b_r + out_c_r + h_r + w_x) += res;
                        }

// std::cout << "[time]: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count() << " ms" << std::endl;

        for(int b = 0, i = 0; b < batch_size; b++)
            for(int out_c = 0; out_c < out_channels; out_c++)
                for(int out_h = 0; out_h < out_height; out_h++)
                    for(int out_w = 0; out_w < out_width; out_w++, i++)
                        result[b][out_c][out_h][out_w] = (*(_result + i));


        delete[] _result;
        delete[] _X;
        delete[] _W;

        return result;
    }

//     Mat2d::mat2d forward(const Mat2d::mat2d &X)
//     {
//         if(padding != 0)
//             this->X = add_padding(X, padding);
//         else
//             this->X = Mat2d::mat2d(X);

//         Mat2d::size_t x_size = Mat2d::size(this->X);
//         int batch_size = x_size[0];
//         int channels = x_size[1];
//         int height = x_size[2];
//         int width = x_size[3];

//         int out_height = (height - filter_size) + 1;
//         int out_width = (width - filter_size) + 1;
//         Mat2d::mat2d result = Mat2d::init(Mat2d::size_t(batch_size, out_channels, out_height, out_width));


// std::cout << in_channels << " " << out_channels << "\n";
// auto begin = std::chrono::steady_clock::now();

//         for(int b = 0; b < batch_size; b++)
//             for(int c_out = 0; c_out < out_channels; c_out++)
//             {
//                 result[b][c_out] = Mat::mat(out_height, Vec::vec(out_width, B->value[0][0][0][c_out]));
//                 for(int c = 0; c < channels; c++)
//                     for(int h_out = 0; h_out < out_height; h_out++)
//                     {
//                         for(int w_out = 0; w_out < out_width; w_out++)
//                         {
//                             double _result = 0;
//                             for(int f1 = h_out, i = 0; f1 < h_out+filter_size; f1++, i++)
//                                 for(int f2 = w_out, j = 0; f2 < w_out+filter_size; f2++, j++)
//                                     _result += this->X[b][c][f1][f2] * W->value[c_out][c][i][j];
//                             result[b][c_out][h_out][w_out] += _result;
//                         }
//                     }
//             }

// std::cout << "[time]: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count() << " ms" << std::endl;

//         return result;
//     }

    Mat2d::mat2d backward(const Mat2d::mat2d &d_out)
    {

    }

    std::vector<std::shared_ptr<Weights2d>> params()
    {
        return std::vector<std::shared_ptr<Weights2d>>{W, B};
    }

private:

    Mat2d::mat2d add_padding(const Mat2d::mat2d &mat, int padding)
    {
        Mat2d::size_t mat_size = Mat2d::size(mat);
        int batch_size = mat_size[0];
        int channels = mat_size[1];
        int height = mat_size[2];
        int width = mat_size[3];

        Mat2d::mat2d result = Mat2d::zeros(Mat2d::size_t(batch_size,
                                                         channels,
                                                         height + (padding * 2), 
                                                         width + (padding * 2)));
        for(int i = 0; i < batch_size; i++)
            for(int j = 0; j < channels; j++)
                for(int k = 0; k < height; k++)
                    for(int l = 0; l < width; l++)
                        result[i][j][k+padding][l+padding] = mat[i][j][k][l];
        return result;
    }

};


#endif